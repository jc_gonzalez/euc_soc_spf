#!/bin/bash
##############################################################################
# File       : lib-console.sh - Functions to show info in console
# Domain     : SPF.scripts
# Version    : 2.0
# Date       : 2020/02/08
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Define some Bash functions to show info in console
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#- Messages
_ONHDR="\e[1;49;93m"
_ONMSG="\e[1;49;92m"
_ONRUN="\e[0;49;32m"
_ONERR="\e[1;49;91m"
_OFF="\e[0m"
STEP=0

#- Other
DATE=$(date +"%Y%m%d_%H%M%S")

get_date() {
    echo $DATE
}

get_build_id() {
    REV_ID=""

    wsvn=$(which svn 2>/dev/null)
    if [ -n "$wsvn" ]; then
	REV_ID=$(svn info ${QPF_PATH} 2>/dev/null | awk '/^Revision:/{print $2;}')
    fi

    if [ -z "$REV_ID" ]; then
	wgit=$(which git 2>/dev/null)
	if [ -n "$wgit" ]; then
            REV_ID=$(git rev-parse HEAD 2>/dev/null)
	fi
    fi

    if [ -z "$REV_ID" ]; then
	REV_ID="NON-TRACKED-WORK-COPY-COMPILATION-00000"
    fi
    echo $REV_ID
}

cmake_setup() {
    cmakefiles_path=${1:-..}
    install_prefix=${2:-$HOME/.local}
    shift 2
    other=$*

    CMAKE_OPTS="-D CMAKE_INSTALL_PREFIX:PATH=${install_prefix}"
    CMAKE_OPTS="$CMAKE_OPTS -DCMAKE_BUILD_TYPE=Debug "
    CMAKE_OPTS="$CMAKE_OPTS --graphviz=dependencies.dot"
    
    cmake ${CMAKE_OPTS} ${other} ${cmakefiles_path}
}

make_build() {
    make -k -j4 $*
}

greetings () {
    title=${1:-This is the title}
    subtitle=${2:-This is the subtitle}
    desc=${3:-Hello, world}
    copy=${4:-Copyright (C) $(date +"%Y") J. C. Gonzalez}
    
    say "${_ONHDR}==============================================================================="
    say "${_ONHDR} $title"
    say "${_ONHDR} $subtitle"
    say "${_ONHDR} $desc"
    say "${_ONHDR} $DATE"
    say "${_ONHDR} $copy"
    say "${_ONHDR}==============================================================================="
    say ""
}

say () {
    echo -e "$*${_OFF}"
    #echo -e "$*" | sed -e 's#.\[[0-9];[0-9][0-9];[0-9][0-9]m##g' >> "${LOG_FILE}"
}

SAY () {
    echo -e "${_ONMSG}$*${_OFF}"
    #echo -e "$*" | sed -e 's#.\[[0-9];[0-9][0-9];[0-9][0-9]m##g' >> "${LOG_FILE}"
}

step () {
    say "${_ONMSG}## STEP ${STEP} - $*"
    STEP=$(($STEP + 1))
}

die () {
    say "${_ONERR}ERROR: $*"
    exit 1
}

