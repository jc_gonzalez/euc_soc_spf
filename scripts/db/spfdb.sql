-- ======================================================================
-- File:     spfdb.sql
--           QPFDB PostgreSQL Database creation script
--
-- Version:  3.0
--
-- Date:     2019/11/14
--
-- Author:   J C Gonzalez
--
-- Copyright (C) 2015-2020 Euclid SOC Team @ ESAC
-- ----------------------------------------------------------------------
-- PostgreSQL database dump
-- - Dumped from database version 10.5
-- - Dumped by pg_dump version 10.5
-- ======================================================================

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

-- ----------------------------------------------------------------------
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


-- ----------------------------------------------------------------------
-- Name: tablefunc; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS tablefunc WITH SCHEMA public;
COMMENT ON EXTENSION tablefunc IS 'functions that manipulate whole tables, including crosstab';


--
-- Name: alert_group; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.alert_group AS ENUM (
    'System',
    'Diagnostics'
);


ALTER TYPE public.alert_group OWNER TO eucops;

--
-- Name: alert_severity; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.alert_severity AS ENUM (
    'Warning',
    'Error',
    'Fatal'
);


ALTER TYPE public.alert_severity OWNER TO eucops;

--
-- Name: alert_type; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.alert_type AS ENUM (
    'General',
    'Resource',
    'OutOfRange',
    'Diagnostic',
    'Comms'
);


ALTER TYPE public.alert_type OWNER TO eucops;

--
-- Name: alert_vartype; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.alert_vartype AS ENUM (
    'INT',
    'FLOAT',
    'DOUBLE'
);


ALTER TYPE public.alert_vartype OWNER TO eucops;

--
-- Name: alert_varvalue; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.alert_varvalue AS (
	i integer,
	f double precision,
	d double precision
);


ALTER TYPE public.alert_varvalue OWNER TO eucops;

--
-- Name: alert_variable; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.alert_variable AS (
	name text,
	x public.alert_varvalue,
	ll public.alert_varvalue,
	ul public.alert_varvalue,
	vtype public.alert_vartype
);


ALTER TYPE public.alert_variable OWNER TO eucops;

--
-- Name: prod_creator_enum; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.prod_creator_enum AS ENUM (
    'SOC_LE1',
    'SOC_LE1_TEST',
    'SOC_QLA_OPE',
    'SOC_QLA_TEST'
);


ALTER TYPE public.prod_creator_enum OWNER TO eucops;

--
-- Name: prod_instrument_enum; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.prod_instrument_enum AS ENUM (
    'VIS',
    'NISP',
    'NIR',
    'SIR',
    'UNKNOWN_INST'
);


ALTER TYPE public.prod_instrument_enum OWNER TO eucops;

--
-- Name: prod_obsmode_enum; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.prod_obsmode_enum AS ENUM (
    'W',
    'C',
    'S',
    'D',
    'NOMINAL',
    'TEST'
);


ALTER TYPE public.prod_obsmode_enum OWNER TO eucops;

--
-- Name: prod_status_enum; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.prod_status_enum AS ENUM (
    'OK',
    'NOTOK'
);


ALTER TYPE public.prod_status_enum OWNER TO eucops;

--
-- Name: qpf_state; Type: TYPE; Schema: public; Owner: eucops
--

CREATE TYPE public.qpf_state AS ENUM (
    'ERROR',
    'OFF',
    'INITIALISED',
    'RUNNING',
    'OPERATIONAL'
);


ALTER TYPE public.qpf_state OWNER TO eucops;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: task_status; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.task_status (
    task_status_id integer NOT NULL,
    status_desc character varying(128)
);


ALTER TABLE public.task_status OWNER TO eucops;

--
-- Name: tasks_info; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.tasks_info (
    id integer NOT NULL,
    task_id character varying(128) NOT NULL,
    task_status_id integer,
    task_progress integer,
    task_exitcode integer,
    task_path character varying(1024),
    task_size integer,
    task_info json,
    registration_time timestamp without time zone,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    task_data json
);


ALTER TABLE public.tasks_info OWNER TO eucops;

--
-- Name: agents_tasks; Type: MATERIALIZED VIEW; Schema: public; Owner: eucops
--

CREATE MATERIALIZED VIEW public.agents_tasks AS
( SELECT (t.task_data #>> '{Info,Agent}'::text[]) AS agent,
    ts.status_desc AS status,
    count(*) AS count
   FROM (public.tasks_info t
     JOIN public.task_status ts ON ((t.task_status_id = ts.task_status_id)))
  WHERE (t.task_status_id <> 4)
  GROUP BY (t.task_data #>> '{Info,Agent}'::text[]), ts.status_desc
  ORDER BY (t.task_data #>> '{Info,Agent}'::text[]), ts.status_desc)
UNION ALL
 SELECT i.agent,
    i.status,
    i.count
   FROM ( VALUES (NULL::text,'SCHEDULED'::text,0), (NULL::text,'FAILED'::text,0), (NULL::text,'FINISHED'::text,0), (NULL::text,'RUNNING'::text,0), (NULL::text,'PAUSED'::text,0), (NULL::text,'STOPPED'::text,0), (NULL::text,'ABORTED'::text,0), (NULL::text,'ARCHIVED'::text,0)) i(agent, status, count)
  WITH NO DATA;


ALTER TABLE public.agents_tasks OWNER TO eucops;

--
-- Name: agents_tasks_spectra; Type: MATERIALIZED VIEW; Schema: public; Owner: eucops
--

CREATE MATERIALIZED VIEW public.agents_tasks_spectra AS
 SELECT crosstab.agent,
    crosstab.aborted,
    crosstab.archived,
    crosstab.failed,
    crosstab.finished,
    crosstab.paused,
    crosstab.running,
    crosstab.scheduled,
    crosstab.stopped
   FROM public.crosstab('select agent,status,count from agents_tasks order by agent,status'::text, 'select distinct status from agents_tasks order by 1'::text) crosstab(agent text, aborted integer, archived integer, failed integer, finished integer, paused integer, running integer, scheduled integer, stopped integer)
  WHERE (((crosstab.agent = ''::text) IS FALSE) AND (crosstab.agent <> '<not-set>'::text))
  WITH NO DATA;


ALTER TABLE public.agents_tasks_spectra OWNER TO eucops;

--
-- Name: alerts; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.alerts (
    alert_id integer NOT NULL,
    creation timestamp without time zone,
    grp public.alert_group,
    sev public.alert_severity,
    typ public.alert_type,
    origin text,
    msgs text,
    file text,
    var public.alert_variable
);


ALTER TABLE public.alerts OWNER TO eucops;

--
-- Name: alerts_alert_id_seq; Type: SEQUENCE; Schema: public; Owner: eucops
--

CREATE SEQUENCE public.alerts_alert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alerts_alert_id_seq OWNER TO eucops;

--
-- Name: alerts_alert_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eucops
--

ALTER SEQUENCE public.alerts_alert_id_seq OWNED BY public.alerts.alert_id;


--
-- Name: configuration; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.configuration (
    created timestamp without time zone NOT NULL,
    filename character varying(256),
    cfg json,
    last_accessed timestamp without time zone
);


ALTER TABLE public.configuration OWNER TO eucops;

--
-- Name: icommands; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.icommands (
    id integer NOT NULL,
    cmd_date timestamp without time zone,
    cmd_source character varying(32),
    cmd_target character varying(32),
    cmd_executed boolean,
    cmd_content text
);


ALTER TABLE public.icommands OWNER TO eucops;

--
-- Name: icommands_id_seq; Type: SEQUENCE; Schema: public; Owner: eucops
--

CREATE SEQUENCE public.icommands_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.icommands_id_seq OWNER TO eucops;

--
-- Name: icommands_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eucops
--

ALTER SEQUENCE public.icommands_id_seq OWNED BY public.icommands.id;


--
-- Name: message_type; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.message_type (
    message_type integer NOT NULL,
    type_desc character varying(128)
);


ALTER TABLE public.message_type OWNER TO eucops;

--
-- Name: messages; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.messages (
    id integer NOT NULL,
    message_id character varying(128) NOT NULL,
    message_type character varying(128) NOT NULL,
    origin character varying(128) NOT NULL,
    destination character varying(128) NOT NULL,
    message_version integer,
    creation_time timestamp without time zone,
    transmission_time timestamp without time zone,
    reception_time timestamp without time zone
);


ALTER TABLE public.messages OWNER TO eucops;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: eucops
--

CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_id_seq OWNER TO eucops;

--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eucops
--

ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;


--
-- Name: node_states; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.node_states (
    node_name character varying(20) NOT NULL,
    node_state character varying(32)
);


ALTER TABLE public.node_states OWNER TO eucops;

--
-- Name: observation_modes; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.observation_modes (
    obsmode_id integer NOT NULL,
    obsmode_desc character varying(128)
);


ALTER TABLE public.observation_modes OWNER TO eucops;

--
-- Name: products_info; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.products_info (
    id integer NOT NULL,
    product_id character varying(256) NOT NULL,
    product_type character varying(128),
    product_version character varying(128),
    product_size bigint,
    product_status_id public.prod_status_enum,
    proc_func character varying(20),
    creator_id public.prod_creator_enum,
    obs_id character varying(128),
    soc_id character varying(128),
    instrument_id public.prod_instrument_enum,
    obsmode_id public.prod_obsmode_enum,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    registration_time timestamp without time zone,
    format character varying(32) NOT NULL,
    url character varying(1024),
    signature character varying,
    report json
);


ALTER TABLE public.products_info OWNER TO eucops;

--
-- Name: prodfilt_checks_ccd; Type: MATERIALIZED VIEW; Schema: public; Owner: eucops
--

CREATE MATERIALIZED VIEW public.prodfilt_checks_ccd AS
 SELECT DISTINCT sec.key AS sec,
    diag.key AS diag,
    "values".key AS "values",
    items.key AS item
   FROM (((((((public.products_info
     CROSS JOIN LATERAL json_each(products_info.report) report_sections(key, value))
     CROSS JOIN LATERAL json_each(report_sections.value) products(key, value))
     CROSS JOIN LATERAL json_each(products.value) ccdq(key, value))
     CROSS JOIN LATERAL json_each(ccdq.value) sec(key, value))
     CROSS JOIN LATERAL json_each(sec.value) diag(key, value))
     CROSS JOIN LATERAL json_each(diag.value) "values"(key, value))
     CROSS JOIN LATERAL json_each("values".value) items(key, value))
  WHERE ((report_sections.key ~~ 'Results'::text) AND (sec.key ~~ 'diagnostics'::text) AND ("values".key !~~ 'name'::text))
  ORDER BY sec.key, diag.key, "values".key, items.key
  WITH NO DATA;


ALTER TABLE public.prodfilt_checks_ccd OWNER TO eucops;

--
-- Name: prodfilt_checks_file; Type: MATERIALIZED VIEW; Schema: public; Owner: eucops
--

CREATE MATERIALIZED VIEW public.prodfilt_checks_file AS
 SELECT DISTINCT sec.key AS sec,
    diag.key AS diag
   FROM (((((public.products_info
     CROSS JOIN LATERAL json_each(products_info.report) report_sections(key, value))
     CROSS JOIN LATERAL json_each(report_sections.value) products(key, value))
     CROSS JOIN LATERAL json_each(products.value) ccdq(key, value))
     CROSS JOIN LATERAL json_each(ccdq.value) sec(key, value))
     CROSS JOIN LATERAL json_each(sec.value) diag(key, value))
  WHERE ((report_sections.key ~~ 'Results'::text) AND (sec.key !~~ 'diagnostics'::text))
  ORDER BY sec.key, diag.key
  WITH NO DATA;


ALTER TABLE public.prodfilt_checks_file OWNER TO eucops;

--
-- Name: products_info_filter; Type: MATERIALIZED VIEW; Schema: public; Owner: eucops
--

CREATE MATERIALIZED VIEW public.products_info_filter AS
 SELECT products_info.id,
    products_info.product_id,
    products_info.start_time,
    products_info.product_type,
    products_info.url,
    products.key AS fits_file,
    ccdq.key AS ccd,
    diag.key AS diag,
    "values".key AS "values",
    "values".value
   FROM ((((((public.products_info
     CROSS JOIN LATERAL json_each(products_info.report) report_sections(key, value))
     CROSS JOIN LATERAL json_each(report_sections.value) products(key, value))
     CROSS JOIN LATERAL json_each(products.value) ccdq(key, value))
     CROSS JOIN LATERAL json_each(ccdq.value) sec(key, value))
     CROSS JOIN LATERAL json_each(sec.value) diag(key, value))
     CROSS JOIN LATERAL json_each(diag.value) "values"(key, value))
  WHERE ((report_sections.key ~~ 'Results'::text) AND (sec.key ~~ 'diagnostics'::text) AND ("values".key !~~ 'name'::text))
UNION ALL
 SELECT products_info.id,
    products_info.product_id,
    products_info.start_time,
    products_info.product_type,
    products_info.url,
    products.key AS fits_file,
    ccdq.key AS ccd,
    diag.key AS diag,
    'values'::text AS "values",
    ((('{"value": '::text || ("values".value)::text) || '}'::text))::json AS value
   FROM ((((((public.products_info
     CROSS JOIN LATERAL json_each(products_info.report) report_sections(key, value))
     CROSS JOIN LATERAL json_each(report_sections.value) products(key, value))
     CROSS JOIN LATERAL json_each(products.value) ccdq(key, value))
     CROSS JOIN LATERAL json_each(ccdq.value) sec(key, value))
     CROSS JOIN LATERAL json_each(sec.value) diag(key, value))
     CROSS JOIN LATERAL json_each(diag.value) "values"(key, value))
  WHERE ((report_sections.key ~~ 'Results'::text) AND (sec.key ~~ 'processing'::text))
  WITH NO DATA;


ALTER TABLE public.products_info_filter OWNER TO eucops;

--
-- Name: products_info_id_seq; Type: SEQUENCE; Schema: public; Owner: eucops
--

CREATE SEQUENCE public.products_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_info_id_seq OWNER TO eucops;

--
-- Name: products_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eucops
--

ALTER SEQUENCE public.products_info_id_seq OWNED BY public.products_info.id;


--
-- Name: pvc; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.pvc (
    id integer NOT NULL,
    name character varying(32),
    version character varying(32),
    date timestamp without time zone,
    counter integer,
    comment character varying(128)
);


ALTER TABLE public.pvc OWNER TO eucops;

--
-- Name: pvc_id_seq; Type: SEQUENCE; Schema: public; Owner: eucops
--

CREATE SEQUENCE public.pvc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pvc_id_seq OWNER TO eucops;

--
-- Name: pvc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eucops
--

ALTER SEQUENCE public.pvc_id_seq OWNED BY public.pvc.id;


--
-- Name: qpf_vars; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.qpf_vars (
    var_name character varying(32) NOT NULL,
    filter character varying(128) NOT NULL,
    var_value character varying(128)
);


ALTER TABLE public.qpf_vars OWNER TO eucops;

--
-- Name: qpfstates; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.qpfstates (
    qpfstate_id integer NOT NULL,
    timestmp timestamp without time zone,
    sessionname text,
    nodename text,
    state public.qpf_state
);


ALTER TABLE public.qpfstates OWNER TO eucops;

--
-- Name: qpfstates_qpfstate_id_seq; Type: SEQUENCE; Schema: public; Owner: eucops
--

CREATE SEQUENCE public.qpfstates_qpfstate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.qpfstates_qpfstate_id_seq OWNER TO eucops;

--
-- Name: qpfstates_qpfstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eucops
--

ALTER SEQUENCE public.qpfstates_qpfstate_id_seq OWNED BY public.qpfstates.qpfstate_id;


--
-- Name: task_inputs; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.task_inputs (
    task_id character varying(128) NOT NULL,
    product_id character varying(256) NOT NULL
);


ALTER TABLE public.task_inputs OWNER TO eucops;

--
-- Name: task_outputs; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.task_outputs (
    task_id character varying(128) NOT NULL,
    product_id character varying(256) NOT NULL
);


ALTER TABLE public.task_outputs OWNER TO eucops;

--
-- Name: task_status_spectra; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.task_status_spectra (
    host_id character varying(64) NOT NULL,
    agent_id character varying(64) NOT NULL,
    running integer,
    waiting integer,
    paused integer,
    stopped integer,
    failed integer,
    finished integer,
    total integer
);


ALTER TABLE public.task_status_spectra OWNER TO eucops;

--
-- Name: tasks_info_id_seq; Type: SEQUENCE; Schema: public; Owner: eucops
--

CREATE SEQUENCE public.tasks_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_info_id_seq OWNER TO eucops;

--
-- Name: tasks_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eucops
--

ALTER SEQUENCE public.tasks_info_id_seq OWNED BY public.tasks_info.id;


--
-- Name: transmissions; Type: TABLE; Schema: public; Owner: eucops
--

CREATE TABLE public.transmissions (
    id integer NOT NULL,
    msg_date timestamp without time zone,
    msg_from character(64),
    msg_to character(64),
    msg_type character(64),
    msg_bcast character(1),
    msg_content text
);


ALTER TABLE public.transmissions OWNER TO eucops;

--
-- Name: transmissions_id_seq; Type: SEQUENCE; Schema: public; Owner: eucops
--

CREATE SEQUENCE public.transmissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transmissions_id_seq OWNER TO eucops;

--
-- Name: transmissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eucops
--

ALTER SEQUENCE public.transmissions_id_seq OWNED BY public.transmissions.id;


--
-- Name: alerts alert_id; Type: DEFAULT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.alerts ALTER COLUMN alert_id SET DEFAULT nextval('public.alerts_alert_id_seq'::regclass);


--
-- Name: icommands id; Type: DEFAULT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.icommands ALTER COLUMN id SET DEFAULT nextval('public.icommands_id_seq'::regclass);


--
-- Name: messages id; Type: DEFAULT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);


--
-- Name: products_info id; Type: DEFAULT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.products_info ALTER COLUMN id SET DEFAULT nextval('public.products_info_id_seq'::regclass);


--
-- Name: qpfstates qpfstate_id; Type: DEFAULT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.qpfstates ALTER COLUMN qpfstate_id SET DEFAULT nextval('public.qpfstates_qpfstate_id_seq'::regclass);


--
-- Name: tasks_info id; Type: DEFAULT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.tasks_info ALTER COLUMN id SET DEFAULT nextval('public.tasks_info_id_seq'::regclass);


--
-- Name: transmissions id; Type: DEFAULT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.transmissions ALTER COLUMN id SET DEFAULT nextval('public.transmissions_id_seq'::regclass);


--
-- Data for Name: alerts; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.alerts (alert_id, creation, grp, sev, typ, origin, msgs, file, var) FROM stdin;
\.


--
-- Data for Name: configuration; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.configuration (created, filename, cfg, last_accessed) FROM stdin;
\.


--
-- Data for Name: icommands; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.icommands (id, cmd_date, cmd_source, cmd_target, cmd_executed, cmd_content) FROM stdin;
\.


--
-- Data for Name: message_type; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.message_type (message_type, type_desc) FROM stdin;
\.


--
-- Data for Name: messages; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.messages (id, message_id, message_type, origin, destination, message_version, creation_time, transmission_time, reception_time) FROM stdin;
\.


--
-- Data for Name: node_states; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.node_states (node_name, node_state) FROM stdin;
\.


--
-- Data for Name: observation_modes; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.observation_modes (obsmode_id, obsmode_desc) FROM stdin;
\.


--
-- Data for Name: products_info; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.products_info (id, product_id, product_type, product_version, product_size, product_status_id, proc_func, creator_id, obs_id, soc_id, instrument_id, obsmode_id, start_time, end_time, registration_time, format, url, signature, report) FROM stdin;
\.


--
-- Data for Name: pvc; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.pvc (id, name, version, date, counter, comment) FROM stdin;
1	LE1_Processor                   	V0.1                            	2016-02-09 14:01:42	1	First version of LE1_processor script, just for testing purposes
2	QLA_Processor                   	V1.0                            	2016-02-09 14:01:42	1	First version of QLA_processor script, just for testing purposes
3	QLA_Processor                   	V1.2                            	2017-01-09 14:01:42	2	First ammend to version 1.0 of QLA_processor script, again just for testing purposes
4	Archive_Ingestor                	V0.1                            	2017-01-09 14:01:42	1	First version of the Archive Ingestor script, just for testing purposes
5	LE1_VIS_Processor               	V0.1                            	2017-05-09 14:01:42	1	First version of LE1_VIS_Processor script, just for testing purposes
6	LE1_NISP_Processor              	V0.1                            	2017-05-09 14:01:42	1	First version of LE1_NISP_Processor script, just for testing purposes
\.


--
-- Data for Name: qpf_vars; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.qpf_vars (var_name, filter, var_value) FROM stdin;
\.


--
-- Data for Name: qpfstates; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.qpfstates (qpfstate_id, timestmp, sessionname, nodename, state) FROM stdin;
\.


--
-- Data for Name: task_inputs; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.task_inputs (task_id, product_id) FROM stdin;
\.


--
-- Data for Name: task_outputs; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.task_outputs (task_id, product_id) FROM stdin;
\.


--
-- Data for Name: task_status; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.task_status (task_status_id, status_desc) FROM stdin;
-2	SCHEDULED
-1	FAILED
0	FINISHED
1	RUNNING
2	PAUSED
3	STOPPED
4	ABORTED
5	ARCHIVED
6	UNKNOWN
\.


--
-- Data for Name: task_status_spectra; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.task_status_spectra (host_id, agent_id, running, waiting, paused, stopped, failed, finished, total) FROM stdin;
\.


--
-- Data for Name: tasks_info; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.tasks_info (id, task_id, task_status_id, task_progress, task_exitcode, task_path, task_size, task_info, registration_time, start_time, end_time, task_data) FROM stdin;
\.


--
-- Data for Name: transmissions; Type: TABLE DATA; Schema: public; Owner: eucops
--

COPY public.transmissions (id, msg_date, msg_from, msg_to, msg_type, msg_bcast, msg_content) FROM stdin;
\.


--
-- Name: alerts_alert_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eucops
--

SELECT pg_catalog.setval('public.alerts_alert_id_seq', 53298, true);


--
-- Name: icommands_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eucops
--

SELECT pg_catalog.setval('public.icommands_id_seq', 1, false);


--
-- Name: messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eucops
--

SELECT pg_catalog.setval('public.messages_id_seq', 1, false);


--
-- Name: products_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eucops
--

SELECT pg_catalog.setval('public.products_info_id_seq', 2992, true);


--
-- Name: pvc_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eucops
--

SELECT pg_catalog.setval('public.pvc_id_seq', 1, false);


--
-- Name: qpfstates_qpfstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eucops
--

SELECT pg_catalog.setval('public.qpfstates_qpfstate_id_seq', 1, true);


--
-- Name: tasks_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eucops
--

SELECT pg_catalog.setval('public.tasks_info_id_seq', 13470, true);


--
-- Name: transmissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eucops
--

SELECT pg_catalog.setval('public.transmissions_id_seq', 1, true);


--
-- Name: alerts alerts_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.alerts
    ADD CONSTRAINT alerts_pkey PRIMARY KEY (alert_id);


--
-- Name: configuration configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.configuration
    ADD CONSTRAINT configuration_pkey PRIMARY KEY (created);


--
-- Name: message_type message_type_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.message_type
    ADD CONSTRAINT message_type_pkey PRIMARY KEY (message_type);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id, message_id, message_type, origin, destination);


--
-- Name: node_states node_states_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.node_states
    ADD CONSTRAINT node_states_pkey PRIMARY KEY (node_name);


--
-- Name: products_info products_info_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.products_info
    ADD CONSTRAINT products_info_pkey PRIMARY KEY (product_id, format);


--
-- Name: qpfstates qpfstates_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.qpfstates
    ADD CONSTRAINT qpfstates_pkey PRIMARY KEY (qpfstate_id);


--
-- Name: task_inputs task_inputs_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.task_inputs
    ADD CONSTRAINT task_inputs_pkey PRIMARY KEY (task_id, product_id);


--
-- Name: task_outputs task_outputs_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.task_outputs
    ADD CONSTRAINT task_outputs_pkey PRIMARY KEY (task_id, product_id);


--
-- Name: task_status task_status_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.task_status
    ADD CONSTRAINT task_status_pkey PRIMARY KEY (task_status_id);


--
-- Name: task_status_spectra task_status_spectra_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.task_status_spectra
    ADD CONSTRAINT task_status_spectra_pkey PRIMARY KEY (agent_id);


--
-- Name: tasks_info tasks_info_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.tasks_info
    ADD CONSTRAINT tasks_info_pkey PRIMARY KEY (task_id);


--
-- Name: transmissions transmissions_pkey; Type: CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.transmissions
    ADD CONSTRAINT transmissions_pkey PRIMARY KEY (id);


--
-- Name: task_inputs task_inputs_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.task_inputs
    ADD CONSTRAINT task_inputs_task_id_fkey FOREIGN KEY (task_id) REFERENCES public.tasks_info(task_id);


--
-- Name: task_outputs task_outputs_task_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.task_outputs
    ADD CONSTRAINT task_outputs_task_id_fkey FOREIGN KEY (task_id) REFERENCES public.tasks_info(task_id);


--
-- Name: tasks_info tasks_info_task_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eucops
--

ALTER TABLE ONLY public.tasks_info
    ADD CONSTRAINT tasks_info_task_status_id_fkey FOREIGN KEY (task_status_id) REFERENCES public.task_status(task_status_id);


--
-- Name: agents_tasks; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: eucops
--

REFRESH MATERIALIZED VIEW public.agents_tasks;


--
-- Name: agents_tasks_spectra; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: eucops
--

REFRESH MATERIALIZED VIEW public.agents_tasks_spectra;


--
-- Name: prodfilt_checks_ccd; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: eucops
--

REFRESH MATERIALIZED VIEW public.prodfilt_checks_ccd;


--
-- Name: prodfilt_checks_file; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: eucops
--

REFRESH MATERIALIZED VIEW public.prodfilt_checks_file;


--
-- Name: products_info_filter; Type: MATERIALIZED VIEW DATA; Schema: public; Owner: eucops
--

REFRESH MATERIALIZED VIEW public.products_info_filter;


--
-- PostgreSQL database dump complete
--

