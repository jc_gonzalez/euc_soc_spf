#!/bin/bash
##############################################################################
# File       : pgsql_start_server.sh - PostgreSQL Server start
# Domain     : SPF.scripts
# Version    : 3.0
# Date       : 2020/05/01
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : PostgreSQL Server start
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

PGCTL=$(locate '/bin/pg_ctl')
PGSPFDIR=/home/eucops/opt/pgsql

# Prepare internal folder
sudo chmod 777 /var/run/postgresql
sudo chown -R postgres:postgres /var/run/postgresql

# Try to start server
${PGCTL} -D ${PGSPFDIR}/data -l logfile start

# Check server status
status=$(${PGCTL} -D ${PGSPFDIR}/data status 2>&1 | head -1 | cut -d" " -f 4)
if [ "$status" == "running" ]; then
    echo "Server is now running"
else
    echo "ERROR: Server could not be started."
    exit 1
fi

