#!/bin/bash
##############################################################################
# File       : pgsql_initdb.sh - PostgreSQL Database initialisation
# Domain     : SPF.scripts
# Version    : 3.0
# Date       : 2020/05/01
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : PostgreSQL Database initialisation
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

PGCTL=$(locate '/bin/pg_ctl')
PGSPFDIR=/home/eucops/opt/pgsql

rm -rf ${PGSPFDIR}/data
mkdir -p  ${PGSPFDIR}/data

${PGCTL} -D ${PGSPFDIR}/data -l ${PGSPFDIR}/logfile initdb && \
    echo "PostgreSQL DB initialized at ${PGSPFDIR}/data" && \
    echo "PostgreSQL DB log file is in ${PGSPFDIR}/logfile"
