#!/bin/bash
##############################################################################
# File       : reset_db.sh - Reset database
# Domain     : SPF.scripts
# Version    : 2.2
# Date       : 2020/02/22
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Compiles and installs the application
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#- Script variables

SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SCRIPTNAME=$(basename "${BASH_SOURCE[0]}")
CURDIR=$(pwd)

#- Import libs
source ${SCRIPTPATH}/lib-console.sh

#- SPF source code paths
SPF_PATH=$(dirname "${SCRIPTPATH}")
BUILD_PATH="${SPF_PATH}"/build
BUILD_ID=$(get_date)__$(get_build_id)
QPF_SQ_SCPT="${SCRIPTPATH}/spfdb.sql"

#- Define usage
usage () {
    say ""
    SAY "$SCRIPTNAME - Resets content of database"
    say ""
    say "Usage:  $SCRIPTNAME  [ -h | <PSQL-opts> ]"
    say ""
    say "where:"
    say "  -h          Show this help message"
    say ""
    exit $1
}

#- Parse command line options
while getopts :hcirB:D: OPT; do
    case $OPT in
        h|+h) usage 0 ;;
        *)    usage 1
    esac
done
shift `expr $OPTIND - 1`
OPTIND=1

PSQL_OPTS="$*"

#### START

#- Say hello

greetings "Euclid SOC Processing Framework" \
          "Reset SPF Database" \
	      "Build id.: $BUILD_ID"

#- Create build

say "Dropping existing database . . ."
psql postgres ${PSQL_OPTS} -q -c "DROP DATABASE qpfdb;" || \
    die "Another program must be using the database"

say "Re-creating database . . ."
(psql postgres ${PSQL_OPTS} -q -c "CREATE DATABASE qpfdb OWNER eucops;" && \
 psql qpfdb    ${PSQL_OPTS} -q -f "${QPF_SQ_SCPT}" 1>/dev/null) || \
    die "Cannot reset database"

#- Cleanup

say "Done."
cd ${CURDIR}
exit 0

