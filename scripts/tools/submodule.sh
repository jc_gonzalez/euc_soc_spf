#!/bin/bash
##############################################################################
# File       : subnmodule.sh - Initialize and setup submodules for compilation
# Domain     : SPF.scripts
# Version    : 2.0
# Date       : 2020/02/08
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Initialize and setup submodules for compilation
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#- Script variables

SCRIPTPATH=$(cd $(dirname $0); pwd; cd - >/dev/null)
SCRIPTNAME=$(basename $0)
CURDIR=$(pwd)

#- Import libs
source ${SCRIPTPATH}/lib-console.sh

#- SPF source code paths
SPF_PATH=$(dirname "${SCRIPTPATH}")
BUILD_ID=$(get_date)__$(get_build_id)

#- Define usage
usage () {
    say ""
    SAY "$SCRIPTNAME - Initialize and setup submodules for compilation"
    say ""
    say "Usage:  $SCRIPTNAME  [ -h ]"
    say ""
    say "where:"
    say "  -h          Show this help message"
    say ""
    exit $1
}

#- Parse command line options
while getopts :hcirB:D: OPT; do
    case $OPT in
        h|+h) usage 0 ;;
        *)    usage 1
    esac
done
shift `expr $OPTIND - 1`
OPTIND=1

#### START

#- Say hello

greetings "Euclid SOC Processing Framework" \
          "Building the SPF application" \
	        "Build id. $BUILD_ID"

#- Update git submodules

cd ${SPF_PATH}
git submodule init .
git submodule update --remote --merge

#- Cleanup

say "Done."
cd ${CURDIR}
exit 0
