#!/bin/bash
##############################################################################
# File       : clone-wa.sh - Clone WA into specified folder
# Domain     : SPF.scripts
# Version    : 2.0
# Date       : 2020/02/08
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Clone WA into specified folder
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#- Script variables

SCRIPTPATH=$(cd $(dirname $0); pwd; cd - >/dev/null)
SCRIPTNAME=$(basename $0)
CURDIR=$(pwd)

#- Import libs
source ${SCRIPTPATH}/lib-console.sh

#- SPF source code paths
SPF_PATH=$(dirname "${SCRIPTPATH}")
WA_SRC_DIR="${SPF_PATH}"/run/spf
BUILD_ID=$(get_date)__$(get_build_id)

#- Define usage
usage () {
    say ""
    SAY "$SCRIPTNAME - Build and install the Euclid SOC Processing Framework"
    say ""
    say "Usage:  $SCRIPTNAME  [ -h ] [ -d <wa-dir> ]"
    say ""
    say "where:"
    say "  -h          Show this help message"
    say "  -d          New Work Area folder"
    say ""
    exit $1
}

#- Parse command line options
WA_TGT_DIR="${HOME}/spf"
while getopts :hd: OPT; do
    case $OPT in
        h|+h) usage 0 ;;
        d|+d) WA_TGT_DIR="$OPTARG" ;;
        *)    usage 1
    esac
done
shift `expr $OPTIND - 1`
OPTIND=1

#### START

#- Say hello

greetings "Euclid SOC Processing Framework" \
          "Clone WA into specified folder" \
	        "Build id.: $BUILD_ID"

#- Place WA in new folder

mkdir -p ${WA_TGT_DIR}
cp -r ${WA_SRC_DIR}/* ${WA_TGT_DIR}/

#- Cleanup

say "Done."
cd ${CURDIR}
exit 0
