#!/bin/bash
##############################################################################
# File       : Build.sh - Compiles and installs the application
# Domain     : SPF.scripts
# Version    : 3.0
# Date       : 2020/05/08
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Compiles and installs the application
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#- Script variables

SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SCRIPTNAME=$(basename "${BASH_SOURCE[0]}")
CURDIR=$(pwd)

#- Import libs
source ${SCRIPTPATH}/../lib/lib-console.sh

#- SPF source code paths
SPF_PATH="$(cd "${SCRIPTPATH}/../.." && pwd)"
DATE=$(get_date)
BUILD_PATH="${SPF_PATH}"/build
BUILD_ID=${DATE}__$(get_build_id)
WA_TGT_DIR="${HOME}/spf"
WA_SRC_DIR="${SPF_PATH}"/run/spf

#- Define usage
usage () {
    say ""
    SAY "$SCRIPTNAME - Build and install the Euclid SOC Processing Framework"
    say ""
    say "Usage:  $SCRIPTNAME  [ -h ] [ -c ] [ -i ] [ -r ] [ -w ] [ -b ] [ -B var ] [ -D define ]"
    say ""
    say "where:"
    say "  -h          Show this help message"
    say "  -c          Compiles the application (default action)"
    say "  -i          Install the application"
    say "  -r          Resets the build process"
    say "  -w          Creates work area"
    say "  -b          Resets SPF database"
    say "  -B          Defines a tag for the Build process"
    say "  -D          Defines a macro for the compilation step"
    say ""
    exit $1
}

#- Parse command line options
COMPILE="yes"
INSTALL="no"
RESET="no"
WA="no"
DB="no"
BUILD_FLAGS=""
COMP_FLAGS=""

while getopts :hcirwbB:D: OPT; do
    case $OPT in
        h|+h) usage 0 ;;
        c|+c) COMPILE="yes" ;;
        i|+i) INSTALL="yes" ;;
        r|+r) RESET="yes" ;;
        w|+w) WA="yes" ;;
        b|+b) DB="yes" ;;
        B|+B) BUILD_FLAGS="${BUILD_FLAGS} -D $OPTARG" ;;
        D|+D) COMP_FLAGS="${COMP_FLAGS} -D$OPTARG" ;;
        *)    usage 1
    esac
done
shift `expr $OPTIND - 1`
OPTIND=1

#### START

#- Say hello

greetings "Euclid SOC Processing Framework" \
          "Building the SPF application" \
	      "Build id.: $BUILD_ID"

#- Create build

if [ "$RESET" == "yes" ]; then
    say "Creating build area . . ."
    rm -rf ${BUILD_PATH}
    mkdir -p ${BUILD_PATH}
fi
cd ${BUILD_PATH}

#- Launch cmake and make

if [ "$RESET" == "yes" ]; then
    say "Configure CMake project . . ."
    OTHER_FLAGS="${BUILD_FLAGS}"
    if [ -n "$COMP_FLAGS" ]; then
	    OTHER_FLAGS="${OTHER_FLAGS} -DCMAKE_C_FLAGS+=\"$COMP_FLAGS\""
	    OTHER_FLAGS="${OTHER_FLAGS} -DCMAKE_CXX_FLAGS+=\"$COMP_FLAGS\""
    fi
    cmake_setup .. $HOME/.local ${OTHER_FLAGS}
fi

if [ "$COMPILE" == "yes" ]; then
    say "Build . . ."
    make
fi

if [ "$INSTALL" == "yes" ]; then
    say "Install . . ."
    make install
fi

if [ "$WA" == "yes" ]; then
    say "Placing work area . . ."
    if [ -d "${WA_TGT_DIR}" ]; then
        mv ${WA_TGT_DIR} ${WA_TGT_DIR}.${DATE}
    fi
    mkdir -p ${WA_TGT_DIR}
    cp -r ${WA_SRC_DIR}/* ${WA_TGT_DIR}/
fi

if [ "$DB" == "yes" ]; then
    say "Initializing SPF data base . . ."
    . ${SCRIPTPATH}/../db/reset_db.sh
fi

## Finishing
step "Final comments"

say "Compilation/Installation finished."
say ""
say "-------------------------------------------------------------------------------"
say "Please, do not forget to:"
say "  - include the directories ${WORK_AREA}/spf/bin and ${HOME}/.local/bin in the PATH variable, and"
say "  - include the directories ${WORK_AREA}/spf/lib and ${HOME}/.local/lib in the LD_LIBRARY_PATH variable."
say "To do that, just execute the following commands:"
say "  export PATH=${WORK_AREA}/spf/bin:${HOME}/.local/bin:${WORK_AREA}/spf/scripts:${WORK_AREA}/spf/scripts/lib:\$PATH"
say "  export LD_LIBRARY_PATH=${WORK_AREA}/spf/lib:${HOME}/.local/lib:${WORK_AREA}/spf/lib64:\$LD_LIBRARY_PATH"

(cat ~/env_spf.sh 2>/dev/null ; echo ""; echo "# Build SPF section") | \
awk '(NR==1),/Build SPF section/' > /tmp/$$.sh
cat /tmp/$$.sh > ~/env_spf.sh
cat <<EOF>> ~/env_spf.sh
export PATH=${WORK_AREA}/spf/bin:${HOME}/.local/bin:${WORK_AREA}/spf/scripts:${WORK_AREA}/spf/scripts/lib:\$PATH
export LD_LIBRARY_PATH=${WORK_AREA}/spf/lib:${HOME}/.local/lib:${WORK_AREA}/spf/lib64:${HOME}/.local/lib64:\$LD_LIBRARY_PATH
EOF

say ""
say "For your convenience, these commands have been saved into the file:"
say "  \$HOME/env_spf.sh"
say "so that you can just update your environment by typing:"
say "  source \$HOME/env_spf.sh"
say ""
say "In order to check that the SPF executable and the libraries were correctly"
say "installed, you may run:"
say "  $ spf --help"
say "and see if the application shows a help message."
say ""
say "Initial configuration files (in JSON) are available at ${WORK_AREA}/spf/cfg."
say "You may need to modify them before launching any processing task from the HMI."
say "(Runing spfhmi without specifying a configuration file will mean reading"
say "last configuration used from internal database)."
say "-------------------------------------------------------------------------------"
say ""

#- Cleanup

say "Done."
cd ${CURDIR}
exit 0
