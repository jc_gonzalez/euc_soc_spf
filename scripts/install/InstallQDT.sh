#!/bin/bash
###############################################################################
# File       : InstallQDT.sh - Script to install QDT to be used by the QPF
# Domain     : QPF.scripts
# Version    : 2.0
# Date       : 2019/02/11
# Copyright (C) 2015-2019 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : QDT installation to be used by QPF
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
#_____________________________________________________________________________
#
# This script tries to simplify the installation of the QDT from a
# working copy of the QDT code located in a folder specified by the
# user, into a QPF Working Area, to be used to process actual or
# simulated LE1 data files.
#
# It performs the following actions:
# - Copy the neccessary files/folders into the appropriate QPF working
#   area subfolder
# - Generate the required config. files (mask files mainly) for the 
#   QDT to operate properly
# - Ensure the QPF-QDT interface elements are correctly configured.
#
###############################################################################

###### Script variables

#- This script path and name
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_NAME=$(basename "${SCRIPT_PATH}")
if [ -h "${SCRIPT_PATH}" ]; then
    while [ -h "${SCRIPT_PATH}" ]; do
        SCRIPT_PATH=$(readlink "${SCRIPT_PATH}")
    done
fi
pushd . > /dev/null
cd $(dirname ${SCRIPT_PATH}) > /dev/null
SCRIPT_PATH=$(pwd)
popd  > /dev/null

#- Messages
_ONHDR="\e[1;49;93m"
_ONMSG="\e[1;49;92m"
_ONRUN="\e[0;49;32m"
_ONERR="\e[1;49;91m"
_OFF="\e[0m"
STEP=0

#- Options
QDTsrc=""
LE1src=""
QDTtgt="${HOME}/qpf/bin"
FORCE=""

#- Other
DATE=$(date +"%Y%m%d%H%M%S")

###### Handy functions

greetings () {
    say "${_ONHDR}==============================================================================="
    say "${_ONHDR} Euclid SOC Processing Framework"
    say "${_ONHDR} Version ${VERSION}"
    say "${_ONHDR} QDT for QPF installation"
    say "${_ONHDR} Execution time-stamp: ${DATE}"
    say "${_ONHDR}==============================================================================="
    say ""
}

usage () {
    local opts="[ -h ] [ -q <QDT-source-path> ] [ -l <LE1-source-path> ] [ -b <qpf/bin folder> ] [ -f ]"
    say "Usage: ${SCRIPT_NAME} $opts"
    say "where:"
    say "  -h         Show this usage message"
    say "  -q <dir>   Specifies where is located the working copy of QDT to use"
    say "  -l <dir>   Specifies where is located the working copy of LE1 emulator to use"
    say "  -b <dir>   Specifies the location where the QDT will be installed"
    say "             (default: ${HOME}/qpf/bin)"
    say "  -f         Force the re-generation of existing config. files"
    say ""
    exit 1
}

say () {
    echo -e "$*${_OFF}"
}

step () {
    say "${_ONMSG}## STEP ${STEP} - $*"
    STEP=$(($STEP + 1))
}

die () {
    say "${_ONERR}ERROR: $*"
    exit 1
}

###### Start

#### Parse command line and display grettings

## Parse command line
while getopts :hq:l:b:f OPT; do
    case $OPT in
        h|+h) usage
            ;;
        q|+q) QDTsrc="$OPTARG"
            ;;
        l|+l) LE1src="$OPTARG"
            ;;
        b|+b) QDTtgt="$OPTARG"
            ;;
	f|+f) FORCE="force"
	    ;;
        *)    usage
            exit 2
            ;;
    esac
done
shift `expr $OPTIND - 1`
OPTIND=1

## Say hello
greetings

## Ensure that the required inputs are provided

if [ -z "$QDTsrc" -o -z "$LE1src" ]; then
    die "Options -q and -l are mandatory"
    usage
    exit 2
fi

## Copy QDT required folders/files to target folder

tgtd="${QDTtgt}/QDT"

step "Creating target folder '$tgtd'"
mkdir -p "$tgtd"

step "Copying the required files/folders"
elements_driver="driver.py"
elements_folders="HKTM_qla NISP_qla VIS_qla common config tools"
for i in ${elements_driver} ${elements_folders} ; do 

    src="$QDTsrc/$i"
    tgt="$tgtd/$i"

    say " - Creating $tgt . . ."
    if [ -d "$tgt" -o -f "$tgt" ]; then 
        rm -rf $tgt
    fi  
    
    cp -r "${src}" "${tgtd}"/
    
done

for i in VIS NISP ; do

    src="$LE1src/convert${i}2QLA.py"
    le1d="${QDTtgt}/LE1_${i}_Processor"
    tgt="$le1d/convert${i}2QLA.py"
    
    say " - Creating $tgt . . ."
    mkdir -p ${le1d}
    cp -r "${src}" "${le1d}"/
    
done

## Generate required config files
declare -a cfg_elements=( \
    vis_specs.txt \
    vis_bpm.fits \
    vis_sat_map.fits \
    nisp_specs.txt \
    nisp_bpm.fits \
    nisp_dark.fits \
    nisp_sat_map.fits \
    hktm_specs.txt \
    psf12x.fits \
    gauss10x.fits )

declare -a cfg_generators=( \
    create_vis_specs.py \
    create_vis_bpm.py \
    create_vis_sat_map.py \
    create_nisp_specs.py \
    create_nisp_bpm.py \
    create_nisp_dark.py \
    create_nisp_sat_map.py \
    create_aocs_specs.py \
    none \
    none )

curdir=$(pwd)
cd $tgtd/config

step "Generating config files"

i=0
while [ $i -lt ${#cfg_elements[@]} ] ; do 
    
    elem="$tgtd/config/${cfg_elements[i]}"
    generator="$tgtd/tools/${cfg_generators[i]}"
    
    say " - Looking for $elem . . ."
    
    gen="yes"
    if [ -f ${elem} ]; then
        say "   File was found"
        gen="no"
        if [ -n "$FORCE" ]; then
            rm -f ${elem}
            gen="yes"
            say "   Removing old file, re-generation will take place"
        fi
    else
        say "   File not found, generation is needed"
    fi
    
    if [ "$gen" == "yes" ]; then
        say "   Generating new $elem . . ."
        time python $generator 
    fi
    
    i=$(( i + 1 ))
    
done

## Ensuring QDT is properly accessed from the QPF

step "Ensuring QDT is properly accessed from the QPF"

for i in VIS NISP; do

    qla=QLA_${i}_Processor
    cd ${QDTtgt}/${qla}
    ln -sf ../QDT/driver.py .
    for j in  ${elements_folders} ; do
        ln -sf ../QDT/$j .
    done
    ln -sf sample-${i}-emu.cfg.json sample.cfg.json

    le1=LE1_${i}_Processor
    cd ${QDTtgt}/${le1}
    ln -sf convert${i}2QLA.py driver.py
    ln -sf sample-${i}-emu.cfg.json sample.cfg.json
    
done

cd ${curdir}

exit 0

