#!/bin/bash
##############################################################################
# File       : InstallCOTS.sh - COTS Installation and database initialisation
# Domain     : SPF.scripts
# Version    : 3.0
# Date       : 2020/04/01
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : COTS Installation and database initialisation
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

###### Script variables

#- This script path and name
SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SCRIPTNAME=$(basename "${BASH_SOURCE[0]}")

status=0

source ${SCRIPTPATH}/../lib/lib-console.sh

#- Options
QT="no"
PGSQL="no"
L4CPP="no"
CURL="no"
UUID="no"
DOCKER="no"
ENVSPF="no"

#- Other
DATE=$(get_date)
LOG_FILE=./cots_${DATE}.log
VERSION=$(cat "${SCRIPTPATH}/../VERSION")
LDLIBS=$(echo $LD_LIBRARY_PATH | tr ':' ' ')

###### Handy functions

usage () {
    local opts="[ -h ] [ -A ] [ -a ] [ -q ] [ -p ] [ -c ] [ -u ] [ -l ] [ -d ] [ -e ]"
    say "Usage: ${SCRIPTNAME} $opts"
    say "where:"
    say "  -h         Show this usage message"
    say "  -A         Install all COTS, INCLUDING Qt framework"
    say "  -a         Install all COTS, EXCLUDING Qt framework"
    say "  -q         Install Qt framework"
    say "  -p         Install PostgreSQL, initialize database and restart server"
    say "  -c         Install libcurl"
    say "  -u         Install UUID library"
    say "  -l         Install log4cpp"
    say "  -d         Install Docker"
    say "  -e         Regenerate env_spf.sh file (default if anything is installed)"
    say ""
    exit 1
}

###### Start

#### Parse command line and display grettings

## Parse command line
while getopts :hAaqplcude OPT; do
    case $OPT in
        h|+h) usage
              ;;
        A|+A) QT="yes"
              PGSQL="yes"
              L4CPP="yes"
              CURL="yes"
              UUID="yes"
              DOCKER="yes"
              ENVSPF="yes"
              ;;
        a|+a) QT="no"
              PGSQL="yes"
              L4CPP="yes"
              CURL="yes"
              UUID="yes"
              DOCKER="yes"
              ENVSPF="yes"
              ;;
        q|+q) QT="yes"
              ;;
        p|+p) PGSQL="yes"
              ;;
        l|+l) L4CPP="yes"
              ;;
        c|+c) CURL="yes"
              ;;
        u|+u) UUID="yes"
              ;;
        d|+d) DOCKER="yes"
              ;;
        e|+e) ENVSPF="yes"
              ;;
        *)    usage
              exit 2
              ;;
    esac
done
shift `expr $OPTIND - 1`
OPTIND=1

## Say hello
greetings "Euclid SOC Processing Framework" \
          "Version ${VERSION}" \
          "COTS Installation and database initialisation"

## Checking required applications and libraries are installed

#### First, enable EPEL repository

if [ \( "${PGSQL}" = "yes" \) -o \( "${QT}" = "yes" \) -o \( "${L4CPP}" = "yes" \) -o \
     \( "${CURL}" = "yes" \) -o \( "${UUID}" = "yes" \) -o \( "${DOCKER}" = "yes" \) ]; then
    sudo yum install -y epel-release
fi

####------------------------------------------------------------
#### Installing COTS: I - Install PostgreSQL

if [ "${PGSQL}" = "yes" ]; then
    step "Installing PostgreSQL . . ."

    # Check if PostgreSQL is already installed in the system
    isInstalled=$(sudo yum list installed postgre\* >/dev/null 2>&1 && echo yes || echo no)
    if [ "${isInstalled}" = "yes" ]; then
	    # Already installed, take path for installed package
	    PSQL_PTH=$(echo /usr/pgsql*)
	    say "PostgreSQL is already installed in ${PSQL_PTH}."
	    sudo yum install -y libpq\*
    else
	    # We need to install PostgreSQL and then setup the database

	    # For LODEEN (CentOS based), the currently available PostgreSQL
	    # version by default is 9.2, but we need 9.4+
	    # We install a PGDG for 10, and then install the 10 packages
	    PSQL_PGDG="https://download.postgresql.org/pub/repos/yum/10/redhat/rhel-7.3-x86_64/pgdg-redhat-repo-latest.noarch.rpm"

	    PSQL_PKGS="postgresql10.x86_64"
	    PSQL_PKGS="$PSQL_PKGS postgresql10-devel.x86_64"
	    PSQL_PKGS="$PSQL_PKGS postgresql10-libs.x86_64"
	    PSQL_PKGS="$PSQL_PKGS postgresql10-server.x86_64"
	    PSQL_PKGS="$PSQL_PKGS postgresql10-docs.x86_64"
        PSQL_PKGS="$PSQL_PKGS postgresql10-contrib.x86_64"

	    #- 1. Installing PostgreSQL
	    say ". Installing packages"
	    sudo yum install -y ${PSQL_PGDG}
	    sudo yum install -y ${PSQL_PKGS}
        sudo yum install -y libpq\*

	    PSQL_PTH=/usr/pgsql-10

        isInstalled="yes"
    fi

    #- 2. Setting up the database

    # Give access to internal folder:
    say ". Preparing internal folders"
    sudo chmod 777 /var/run/postgresql
    sudo chown postgres.postgres /var/run/postgresql
    sudo mkdir -p /opt

    if [ "${PSQL_PTH}" != "/usr/pgsql" ]; then
        sudo rm /usr/pgsql
	    sudo ln -fs ${PSQL_PTH} /usr/pgsql
    fi
    sudo ln -fs ${PSQL_PTH} /opt/pgsql

    PSQL_PTH=/usr/pgsql
    export PATH=$PATH:${PSQL_PTH}/bin

    sudo ln -fs ${PSQL_PTH}/pg_ctl /usr/bin/pgctl

    # Then, for the creation of the local folder, initialization and server start,
    # use the scripts =scripts/pgsql_start_server.sh= and =scripts/pgsql_initdb.sh=

    sudo -u postgres createuser -s eucops

    if [ "${isInstalled}" = "yes" ]; then
	    say ". Initializing database"
	    source ${SCRIPTPATH}/db/pgsql_initdb.sh
	    say ". Starting server"
	    source ${SCRIPTPATH}/db/pgsql_start_server.sh
    fi
fi

####------------------------------------------------------------
#### Installing COTS: II - Install Qt

if [ "${QT}" = "yes" ]; then
    step "Installing Qt framework . . ."

    # Check if PostgreSQL is already installed in the system
    isInstalled=$(sudo yum list installed qt5-\*x86_64 >/dev/null 2>&1 && \
                      echo yes || echo no)
    if [ "${isInstalled}" = "yes" ]; then
	    # Already installed, take path for installed package
	    QT5_PTH=$(echo /usr/lib64/qt5)
    else
	    # Create QT5 list of packages
	    sudo yum -y list qt5-*x86_64 | \
            grep -v -- -examples | grep qt5- | \
            awk '{print $1;}' | tee /tmp/qt5pkgs.list

	    # Install packages
	    sudo yum -y install --skip-broken $(cat /tmp/qt5pkgs.list) harfbuzz-devel.x86_64

	    QT5_PTH=$(echo /usr/lib64/qt5)
    fi
fi

####------------------------------------------------------------
#### Installing COTS: III - Install curl

if [ "${CURL}" = "yes" ]; then
    step "Installing Curl library . . ."
    sudo yum -y install curl.x86_64 libcurl-devel.x86_64
fi

####------------------------------------------------------------
#### Installing COTS: III - Install log4cpp

if [ "${L4CPP}" = "yes" ]; then
    step "Installing Log4Cpp library . . ."
    sudo yum -y install log4cpp-devel.x86_64
fi

####------------------------------------------------------------
#### Installing COTS: IV - Install UUID

if [ "${UUID}" = "yes" ]; then
    step "Installing UUID library . . ."
    sudo yum -y install uuid-devel.x86_64 libuuid-devel.x86_64
fi

####------------------------------------------------------------
#### Installing COTS: V - Install Docker

if [ "${DOCKER}" = "yes" ]; then
    step "Installing Docker"
    say ". Installing packages"
    sudo yum check-update
    curl -fsSL https://get.docker.com/ | sh
    say ". Adding user to docker group"
    sudo usermod -aG docker eucops
    say ". Starting Docker service"
    sudo systemctl start docker
    sleep 2
    sudo systemctl status docker
    sudo systemctl enable docker
fi

#### Finishing
step "Final comments"

cat <<EOF> ${HOME}/env_spf.sh
#!/bin/bash
# Mini-script to update PATH and LD_LIBRARY_PATH environment variables
# for the compilation/execution of Euclid SPF
# Creation date: ${DATE}

#-- InstallCOTS section
export PATH=${QT5_PTH}/bin:/usr/local/bin:${PSQL_PTH}/bin:\$PATH
export LD_LIBRARY_PATH=/usr/lib64:/usr/local/lib:/usr/local/lib64:${PSQL_PTH}/lib:\$LD_LIBRARY_PATH
EOF

say ""
say "Compilation/Installation finished."
say ""
say "-------------------------------------------------------------------------------"
say "Please, do not forget to include folder /usr/lib64 in the LD_LIBRARY_PATH "
say "variable, and the ${QT5_PTH}/bin folder in the PATH variable, with these"
say "commands:"
say "  export PATH=${QT5_PTH}/bin:/usr/local/bin:${PSQL_PTH}/bin:\$PATH"
say "  export LD_LIBRARY_PATH=/usr/lib64:/usr/local/lib:/usr/local/lib64:${PSQL_PTH}/lib:\$LD_LIBRARY_PATH"
say ""
say "For your convenience, these commands have been saved into the file:"
say "  \$HOME/env_spf.sh"
say "so that you can just update your environment by typing:"
say "  source \$HOME/env_spf.sh"
say ""
say "NOTE: It is strongly encouraged that you logout and login again, in order"
say "      to allow some changes take effect."
say "-------------------------------------------------------------------------------"
say ""
say "Done."

