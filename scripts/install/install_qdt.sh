#!/bin/bash
##############################################################################
# File       : install_qdt.sh - Install QDT in the SPF Working Area
# Domain     : SPF.scripts
# Version    : 2.0
# Date       : 2020/02/22
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Install QDT in the SPF Working Area
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#- Script variables

SCRIPTPATH=$(cd $(dirname $0); pwd; cd - >/dev/null)
SCRIPTNAME=$(basename $0)
CURDIR=$(pwd)

#- Import libs
source ${SCRIPTPATH}/lib-console.sh

#- SPF source code paths
SPF_PATH=$(dirname "${SCRIPTPATH}")
BUILD_ID=$(get_date)__$(get_build_id)

#- Define usage
usage () {
    say ""
    SAY "$SCRIPTNAME - Install QDT in the SPF Working Area"
    say ""
    say "Usage:  $SCRIPTNAME  [ -h ] [ -q <qdt_src_dir> ] [ -w <work_area> ]"
    say ""
    say "where:"
    say "  -h          Show this help message"
    say "  -q          Specifies the QDT source directory"
    say "  -w          Specifies the Work Area main directory"
    say ""
    exit $1
}

#- Parse command line options
QDTDIR=""
WA="${HOME}/spf"
while getopts :hq:w: OPT; do
    case $OPT in
        h|+h) usage 0 ;;
        q|+q) QDTDIR="$OPTARG" ;;
        w|+w) WA="$OPTARG" ;;
        *)    usage 1
    esac
done
shift `expr $OPTIND - 1`
OPTIND=1

if [ -z "$QDTDIR" ]; then
    usage 1
fi

#### START

#- Say hello

greetings "Euclid SOC Processing Framework" \
          "Install QDT in the SPF Working Area" \
	        "Build id.: $BUILD_ID"

#- Copy fundamental files and folders

cd ${WA}/bin
if [ -d QDT ]; then
    mv QDT QDT-prev-$$
fi
mkdir QDT && cd QDT

for i in driver.py common config tools HKTM_qla NISP_qla VIS_qla; do
    cp -r ${QDTDIR}/$i .
done

#- Build maps and param files if non present in config. folder

#. Auxiliary files
declare -A creator

#.. For VIS:
creator['vis_specs.txt']='create_vis_specs.py'
creator['vis_bpm.fits']='create_vis_bpm.py -o OUTPUT_NAME'
creator['vis_sat_map.fits']='create_vis_sat_map.py -o OUTPUT_NAME'

#.. For NISP:
creator['nisp_specs_Y.txt']='create_nisp_specs_Y.py'
creator['nisp_specs_H.txt']='create_nisp_specs_H.py'
creator['nisp_specs_J.txt']='create_nisp_specs_J.py'
creator['nisp_specs_spectrum_B.txt']='create_nisp_specs_sp_B0.py'
creator['nisp_specs_spectrum_R0.txt']='create_nisp_specs_sp_R0.py'
creator['nisp_specs_spectrum_R180.txt']='create_nisp_specs_sp_R180.py'
creator['nisp_specs_spectrum_R270.txt']='create_nisp_specs_sp_R270.py'
creator['nisp_dark.fits']='create_nisp_dark.py -o OUTPUT_NAME'
creator['nisp_sat_map.fits']='create_nisp_sat_map.py -o OUTPUT_NAME'
creator['nisp_linearity_map.fits']='create_nisp_linearity_map.py -o OUTPUT_NAME'

#.. For AOCS:
creator['hktm_specs.txt']='create_aocs_specs.py'

#. Files that must exist:

#.. For VIS
#... To run sextractor you need the following files:
mandatory="vis_pysex.sex vis_pysex.param vis_pysex.conv vis_pysex.nnw"
#... To run scamp you need:
mandatory="$mandatory vis_scamp.par euclid_ref_cat.fits nisp_CrRejection.conf"

#.. For NISP
#... To run sextractor you need the following files:
mandatory="$mandatory nisp_Y_pysex.sex nisp_H_pysex.sex nisp_J_pysex.sex"
mandatory="$mandatory nisp_pysex.param nisp_pysex.conv nisp_pysex.nnw"
#... To run scamp you need:
mandatory="$mandatory nisp_scamp.par euclid_ref_cat.fits"

#.. For AOCS:
mandatory="$mandatory psf12x.fits gauss10x.fits"

cd "${WA}/bin/QDT"

# Checking files that are needed and can be re-created
for f in "${!creator[@]}"; do
    ff="config/$f"
    if [ ! -f "$ff" ]; then
        crtr=$(echo "${WA}/bin/QDT/tools/${creator[$f]}" | sed -e "s#OUTPUT_NAME#$ff#g")
        say "File $ff missing.  It will be re-created."
        say "Creating file $ff . . ."
        python3 $crtr
    else
        say "File $ff found . . ."
    fi
done

# Checking files that are needed and can be re-created
for f in $mandatory; do
    ff="config/$f"
    if [ ! -f "$ff" ]; then
        die "Mandatory file $ff missing."
    else
        say "File $ff found . . ."
    fi
done

#- Cleanup

say "Done."
cd ${CURDIR}
exit 0
