#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Launcher for LE1 VIS Processor1

The LE1 VIS Processor, running in a Docker container, must be invoked
in a manner slightly different from the QDT.  Therefore, we use this
launcher to adapt the inputs, and handle the outputs.
"""
#----------------------------------------------------------------------

import os, sys

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

#----------------------------------------------------------------------

import argparse
import logging

logger = logging.getLogger()

import subprocess
import shutil
from datetime import datetime

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

LE1VISProcessor = "/home/user/bin/LE1_VIS_Reader"

def configureLogs(level):
    logger.setLevel(level)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(level)

    # Create formatters and add it to handlers
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    if 'LOGGING_MODULES' in os.environ:
        for lname in os.environ['LOGGING_MODULES'].split(':'):
            lgr = logging.getLogger(lname)
            if not lgr.handlers: lgr.addHandler(c_handler)


def getArgs():
    """
    Parse arguments from command line
    :return: args structure
    """
    parser = argparse.ArgumentParser(description='LE1 VIS Processor launcher',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', dest='ifile', required=True,
                        help='Input files to process')
    parser.add_argument('-m', '--mdb', dest='mdb', required=True,
                        help='File name (without path) of the MDB XML file')
    parser.add_argument('-d', '--debug', dest='debug', default=False, action='store_true',
                        help='Show debug information')
    parser.add_argument('-r', '--rename', dest='rename', default=False, action='store_true',
                        help='Rename output files to follow Euclid File Naming Conventions')

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    return parser.parse_args()


def prepareFiles(ifile, mdbfile):
    """
    Prepare input, output, log... files to invoke processor
    :param ifile: the input file
    :param mdbfile: the full path name of the MDB XML file
    :return: a dictionary with the different files to use
    """
    filedir = os.path.realpath(ifile)
    fname = os.path.basename(filedir)
    dname = os.path.dirname(filedir)
    ddname = os.path.dirname(dname)
    (fbase, fext) = os.path.splitext(fname)
    return ( ddname, { "rawfile": filedir,
                       "fitsfile": os.path.join(ddname, 'out', f'{fbase}.fits'),
                       "xmlfile": os.path.join(ddname, 'out', f'{fbase}.xml'),
                       "log-file": os.path.join(ddname, 'log', f'{fbase}.log'),
                       "mdbfile": mdbfile } )


def prepareCmdLine(files):
    """
    Prepare command line to execute
    :param files: the structure with files names to use
    :return: the command line
    """
    cmdLineOpts = ' '.join([f'--{k} {v}' for k,v in files.items()])
    cmdLine = f'{LE1VISProcessor} {cmdLineOpts}'
    return cmdLine

def launchProcessor(cmd, workdir, shell=False):
    """
    Run command
    :param cmd: The actual command
    :param workdir: The working directory to execute the command
    :param shell: True to activate shell in subprocess
    :return: True on successful execution, False otherwise
    """
    id = f'{os.getpid():05d}'
    try:
        logFile = os.path.join(os.getenv('HOME'), 'Work', f'{id}.log')
        errFile = os.path.join(os.getenv('HOME'), 'Work', f'{id}.err')
        if not shell: cmd = cmd.split(' ')
        logger.debug('Running: {} (shell={})'.format(cmd, shell))
        with open(logFile, 'a') as flog:
            p = subprocess.run(cmd, cwd=workdir,
                               stdout=flog, stderr=subprocess.PIPE, shell=shell)
        if p.stderr:
            with open(errFile, 'a') as ferr:
                ferr.write(p.stderr.decode("utf-8"))
        return True
    except Exception as ee:
        logger.exception('{}'.format(ee))
        logger.error('Running in ')
        return False


def getTagValue(s):
    """
    Get the value of a tag in a line, in the form <tag>value</tag>
    """
    return str(s.split('>')[1]).split('<')[0]


def renameFiles(files):
    """
    Rename output files according to File Naming Conventions
    """
    with open(files['xmlfile'], 'r') as fxml:
        for line in fxml:
            if '<CreationDate>' in line: creationDate = getTagValue(line)
            elif '<Category>' in line: category = getTagValue(line)
            elif '<FirstType>' in line: firstType = getTagValue(line)
            elif '<SecondType>' in line: secondType = getTagValue(line)
            elif '<ThirdType>' in line: thirdType = getTagValue(line)
            elif '<ObservationId>' in line: obsId = getTagValue(line)
            elif '<Exposure>' in line: expos = getTagValue(line)

    logger.debug([creationDate, category, firstType, secondType, thirdType, obsId, expos])

    timestamp = datetime.strptime(creationDate, "%Y-%m-%dT%H:%M:%SZ").strftime("%Y%m%dT%H%M%S.000Z")
    mode = 'D' if thirdType == "DEEP" else ('C' if thirdType == "CALIBRATION" else 'W')
    categ = (f'{category}-' if category != 'SCIENCE' else '')
    itype = (firstType if firstType != 'NULL' else '')
    isubtype = (f'-{secondType}' if secondType != 'NULL' else '')
    descrip = f'{categ}{itype}{isubtype}'
    desc = f'-{descrip}' if len(descrip) > 0 else ''

    eucname = f'EUC_LE1_VIS-{int(obsId):05d}-{expos}-{mode}{desc}_{timestamp}_01.00'
    logger.debug(eucname)

    for f in ['fitsfile', 'xmlfile', 'log-file']:
        orig = files[f]
        fname = os.path.basename(orig)
        dname = os.path.dirname(orig)
        (fbase, fext) = os.path.splitext(fname)
        newName = os.path.join(dname, f'{eucname}{fext}')
        shutil.move(orig, newName)
        logger.info(f'Renaming {orig} to {newName}')


def main():
    """
    Main program
    """
    args = getArgs()
    configureLogs(logging.INFO if not args.debug else logging.DEBUG)

    logger.info('Start...')

    workdir, files = prepareFiles(args.ifile, args.mdb)
    cmdLine = prepareCmdLine(files)
    success = launchProcessor(cmdLine, workdir)

    if not success:
        logger.error('ERROR Running LE1 VIS Processor')
        logger.error(f'Cmd.line was: {cmdLine}')

    if args.rename: renameFiles(files)

    logger.info('Done.')


if __name__ == '__main__':
    main()
