#!/bin/bash
##############################################################################
# File       : CreateDockerImgs.sh - Generates docker images from Dockerfiles
# Domain     : QPF.run.dockerfiles
# Version    : 2.0
# Date       : 2018/02/08
# Copyright (C) 2015-2018  J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Generates docker images from Dockerfiles
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

###### Script variables

#- This script path and name
SCRIPT_PATH="${BASH_SOURCE[0]}";
SCRIPT_NAME=$(basename "${SCRIPT_PATH}")
if [ -h "${SCRIPT_PATH}" ]; then
    while [ -h "${SCRIPT_PATH}" ]; do
        SCRIPT_PATH=$(readlink "${SCRIPT_PATH}")
    done
fi
pushd . > /dev/null
cd $(dirname ${SCRIPT_PATH}) > /dev/null
SCRIPT_PATH=$(pwd)
popd  > /dev/null

LOG_FILE=${SCRIPT_PATH}/${SCRIPT_NAME}.log

#- Script variables

# NEXUS DOCKER REPOSITORY URL
NEXUS_DOCKER_URL="scidockreg.esac.esa.int:60400"
NEXUS_USER="eucops"

DOMAINS="QDT,QDTsrv,LE1sim"
GENERATE="no"
PUSH="no"
RETRIEVE="no"
IMG_NAMES=""

#- Usage
usage () {
    echo "Usage: "
    echo "  ${0##*/} [ -d <domains> ] [ -u <user> ] [ -n <nexus-url> ] [ -g ] [ -t ] [ -p ]"
    echo "where: "
    echo "  -d <domains>    Comma separated list of domains to generate the images"
    echo "                  Default: ${DOMAINS}"
    echo "  -u <user>       User area in Nexus repository to store the images"
    echo "                  Default: ${NEXUS_USER}"
    echo "  -n <nexus-url>  Nexus repository main URL"
    echo "                  Default: ${NEXUS_DOCKER_URL}"
    echo "  -i <img,img>    Provides a comma-separated list of image names (without the"
    echo "                  'Dockerfile__' prefix) to use, instead those listed in the files"
    echo "                  'docker-images.lst'"
    echo "  -g              Generate the images (Default: no)"
    echo "  -p              Push the images to the Nexus repository (Default: no)"
    echo "  -r              Retrieve the images from the Nexus repository.  This option"
    echo "                  resets any -g or -p option before (Default: no)"
}

#- Process command line options
while getopts :u:d:n:i:gtprh OPT; do
    case $OPT in
        u|+u)
            NEXUS_USER="$OPTARG"
            ;;
        d|+d)
            DOMAINS="$OPTARG"
            ;;
        n|+n)
            NEXUS_DOCKER_URL="$OPTARG"
            ;;
        i|+i)
            IMG_NAMES="$OPTARG"
            ;;
        g|+g)
            GENERATE="yes"
            RETRIEVE="no"
            ;;
        p|+p)
            PUSH="yes"
            RETRIEVE="no"
            ;;
        r|+r)
            GENERATE="no"
            PUSH="no"
            RETRIEVE="yes"
            ;;
        h|+h)
            usage
            exit 0
            ;;
        *)
            usage
            exit 2
    esac
done
shift $(( OPTIND - 1 ))
OPTIND=1

#- Loop on Domains
for domain in $(echo ${DOMAINS} | tr ',' ' '); do

    #- Switch to domain
    cd ${domain}
    
    if [ -n "$IMG_NAMES" ]; then
        imgs=$(echo $IMG_NAMES | tr ',' ' ')
    else
        imgs=$(cat docker-images.lst)
    fi
    
    for imgName in ${imgs}; do

        dckFile="Dockerfile__${imgName}"
        if [ ! -f ./${dckFile} ]; then
            continue
        fi
    
        echo "###______ PROCESSING FILE ${dckFile} ______________________________"

        imgId=$(echo ${imgName} | cut -d: -f1)
        imgTag=$(echo ${imgName} | cut -d: -f2)
        
        tagDigit=$(( $(echo ${imgTag} | cut -c1) + 0 ))

        if [ "$GENERATE" = "yes" ]; then        
            #- Generate image
            echo "####### GENERATING IMAGE ${imgName}"
            make -k -f ${SCRIPT_PATH}/Makefile.img -C ${SCRIPT_PATH}/${domain} \
                 DOCKERFILE=${dckFile} IMAGE_NAME=${imgName} clean image
        fi
        
        if [ "$PUSH" = "yes" ]; then        
            echo "####### TAGGING IMAGE"
            docker tag ${imgName} ${NEXUS_DOCKER_URL}/${NEXUS_USER}/${imgName}

            #- Push images
            echo "####### PUSH IMAGES TO NEXUS"
            docker push ${NEXUS_DOCKER_URL}/${NEXUS_USER}/${imgName}
            if [ -n "$latestImgName" ]; then
                docker push ${NEXUS_DOCKER_URL}/${NEXUS_USER}/${latestImgName} 
                docker push ${NEXUS_DOCKER_URL}/${NEXUS_USER}/${latestImgNameTag}
            fi
        fi

        if [ "$RETRIEVE" = "yes" ]; then
            docker pull ${NEXUS_DOCKER_URL}/${NEXUS_USER}/${imgName}
            
            echo "####### TAGGING IMAGE"
            docker tag ${NEXUS_DOCKER_URL}/${NEXUS_USER}/${imgName} ${imgName} 
        fi
           
        if [ "$PUSH" = "yes" -o "$RETRIEVE" = "yes" ]; then
            if [ ${tagDigit} -gt 0 ]; then
                echo "####### TAGGING LATEST IMAGE"
                latestImgName=${imgName}
                latestImgNameTag=${imgId}:latest
                docker tag ${latestImgName} ${latestImgNameTag} 
                docker tag ${NEXUS_DOCKER_URL}/${NEXUS_USER}/${latestImgName} \
                           ${NEXUS_DOCKER_URL}/${NEXUS_USER}/${latestImgNameTag}
            fi
        fi
    done
    
    #- back
    cd ..
    
done

