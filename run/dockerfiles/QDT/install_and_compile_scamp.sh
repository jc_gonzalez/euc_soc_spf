#!/bin/bash
#==============================================================================
# DESCRIPTION:    Compile and install Atlas, FFTW and Scamp
# AUTHOR:         J. C. Gonzalez <JCGonzalez@sciops.esa.int>
# COMMENTS:
#   This script installs ATLAS, FFTW and scamp.
#   It installs the specified versions, from the files in the same folder.
#==============================================================================

# Base Docker image
# Set environment variables
export COTS_ROOT="${AFS_BASE_PATH}/opt"
export COTS_PKG_DIR="${AFS_BASE_PATH}/opt/pkg"

export ATLAS_PKG="atlas3.10.3.tar.bz2"
export ATLAS_SRC="$COTS_ROOT/ATLAS"
export ATLAS_BIN="$COTS_ROOT/atlas.bin"

export FFTW_PKG="fftw-3.3.8.tar.gz"
export FFTW_SRC="$COTS_ROOT/fftw-3.3.8"
export FFTW_BIN="$COTS_ROOT/fftw.bin"

export SCAMP_PKG="scamp-master.zip"
export SCAMP_SRC="$COTS_ROOT/scamp-master"
export SCAMP_BIN="$COTS_ROOT/scamp.bin"

export CURL_LIB="/usr/lib/x86_64-linux-gnu"
export CURL_INC="/usr/include/x86_64-linux-gnu/curl"

CURDIR=$(pwd)
OS="centos"
INSTALL_ATLAS="no"
INSTALL_FFTW="no"
INSTALL_SCAMP="no"

while getopts :afso: OPT; do
    case $OPT in
        a|+a)
            INSTALL_ATLAS="yes" ;;
        f|+f)
            INSTALL_FFTW="yes" ;;
        s|+s)
            INSTALL_SCAMP="yes" ;;
        o|+o)
            OS=${OPTARG} ;;
        *)
            echo "usage: ${0##*/} [ -a ] [ -f ] [ -s ] "
            exit 2
    esac
done
shift $(( OPTIND - 1 ))
OPTIND=1

# Install automake tools, libcurl for dev., gfortran and zip/unzip
echo ""
echo ">>>> Updating packaging system and installing required devel. packages . . ."
apt-get update
apt-get install -y automake libtool checkinstall
if [ "$OS" == "ubuntu" ]; then
    apt-get install -y libcurl4 libcurl4-gnutls-dev
else
    apt-get install -y libcurl3 libcurl3-dev
fi
apt-get install -y gfortran
apt-get install -y zip unzip
mkdir -p ${COTS_ROOT}
mkdir -p ${COTS_PKG_DIR}
echo "" && echo ""
echo ">>>> Installing additional packages . . . "

cd ${CURDIR}

if [ "$INSTALL_ATLAS" == "yes" ]; then
    # Compile and install ATLAS
    cp $ATLAS_PKG $COTS_PKG_DIR/
    echo ""
    echo "============================================================"
    echo "==== Compiling and installing ATLAS ===="
    cd $COTS_ROOT && tar xvjf $COTS_PKG_DIR/$ATLAS_PKG
    mkdir -p $ATLAS_BIN && mkdir -p $ATLAS_SRC/build && cd $ATLAS_SRC/build
    $ATLAS_SRC/configure --prefix=$ATLAS_BIN --cripple-atlas-performance --shared
    make -j16
    make install
    rm -rf $COTS_PKG_DIR/$ATLAS_PKG $ATLAS_SRC

    cd ${CURDIR}
fi

if [ "$INSTALL_FFTW" == "yes" ]; then
    # Compile and install FFTW
    cp $FFTW_PKG $COTS_PKG_DIR/
    echo ""
    echo "============================================================"
    echo "==== Compiling and installing FFTW ===="
    cd $COTS_ROOT && tar xvzf $COTS_PKG_DIR/$FFTW_PKG
    mkdir -p $FFTW_BIN && cd $FFTW_SRC
    ./configure --prefix=$FFTW_BIN --enable-single --enable-threads
    make -j8
    make install
    rm -rf $COTS_PKG_DIR/$FFTW_PKG $FFTW_SRC

    cd ${CURDIR}
fi

if [ "$INSTALL_SCAMP" == "yes" ]; then
    # Compile and install SCAMP
    cp $SCAMP_PKG $COTS_PKG_DIR/
    echo ""
    echo "============================================================"
    echo "==== Compiling and installing SCAMP ===="
    cd $COTS_ROOT && unzip $COTS_PKG_DIR/$SCAMP_PKG
    mkdir -p $SCAMP_BIN && cd $SCAMP_SRC
    sh autogen.sh
    ./configure --prefix=$SCAMP_BIN \
        --with-atlas-libdir=$ATLAS_BIN/lib \
        --with-atlas-incdir=$ATLAS_BIN/include \
        --with-fftw-libdir=$FFTW_BIN/lib \
        --with-fftw-incdir=$FFTW_BIN/include \
        --with-curl-libdir=$CURL_LIB \
        --with-curl-incdir=$CURL_INC
    make
    make install
    rm -rf $COTS_PKG_DIR/$SCAMP_PKG $SCAMP_SRC

    cd ${CURDIR}
fi

# CLeanup and update environment
export PATH="${SCAMP_BIN}/bin:${ATLAS_BIN}/bin:${FFTW_BIN}/bin:${PATH}"
export LD_LIBRARY_PATH="${ATLAS_BIN}/lib:${FFTW_BIN}/lib:${PATH}"
