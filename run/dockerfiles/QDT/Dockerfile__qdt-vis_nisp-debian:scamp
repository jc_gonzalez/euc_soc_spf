#==============================================================================
# VERSION:        2.1
# DESCRIPTION:    Create container with its dependencies for the execution of QDT
# AUTHOR:         J. C. Gonzalez <JCGonzalez@sciops.esa.int>
# COMMENTS:
#   This file describes how to build a container for the execution of the
#   QLA Diagnostic Tools.  The tools are not embeded in the container, but
#   they are rather installed in the host and mapped to internal container
#   paths when the container is executed.
#   This Dockerfile starts from base image and adds the tool scamp
# USAGE:
#   # Build QDT image
#   docker build -t <image:tag> -f <file> .
#
#   # Run interactive
#   docker run -i -t --priviledged=true \
#       -v {procBinPath}:/qlabin \
#       -v {localPath}/:{imgPath} \
#       <image:tag> /bin/bash {imgPath}/{procElem} {imgPath}/dummy.cfg
#
#  Runing script {procElem} assumes the QDT is in /qlabin
#==============================================================================

# Base Docker image
FROM qdt-vis_nisp-debian:base
MAINTAINER J C Gonzalez <JCGonzalez@sciops.esa.int>
ENV QPF_VERSION 2.1

LABEL esa.euclid.soc.qla.content="QDT required package scamp" \
      esa.euclid.soc.qla.version="2.1" \
      esa.euclid.soc.qla.release-date="2019-04-24"
      
# Set environment variables
ENV COTS_ROOT /opt

ENV ATLAS_PKG atlas3.10.3.tar.bz2
ENV ATLAS_SRC $COTS_ROOT/ATLAS
ENV ATLAS_BIN $COTS_ROOT/atlas.bin

ENV FFTW_PKG  fftw-3.3.8.tar.gz
ENV FFTW_SRC  $COTS_ROOT/fftw-3.3.8
ENV FFTW_BIN  $COTS_ROOT/fftw.bin

ENV SCAMP_PKG scamp-master.zip
ENV SCAMP_SRC $COTS_ROOT/scamp-master
ENV SCAMP_BIN $COTS_ROOT/scamp.bin

ENV CURL_LIB /usr/lib/x86_64-linux-gnu
ENV CURL_INC /usr/include/x86_64-linux-gnu/curl

# Install automake tools, libcurl for dev., gfortran and zip/unzip
RUN echo "" && \
    echo ">>>> Updating packaging system and installing required devel. packages . . ." && \
    apt-get update && \
    apt-get install -y automake libtool checkinstall && \
    apt-get install -y libcurl3 libcurl3-dev && \
    apt-get install -y gfortran && \
    apt-get install -y zip unzip && \
    mkdir -p $COTS_ROOT && \
    echo "" && echo "" && \
    echo ">>>> Installing additional packages . . . " 
    
# Compile and install ATLAS
COPY $ATLAS_PKG /
RUN echo "" && \
    echo "============================================================" && \
    echo "==== Compiling and installing ATLAS ====" && \
    cd $COTS_ROOT && tar xvjf /$ATLAS_PKG && \
    mkdir -p $ATLAS_BIN && mkdir -p $ATLAS_SRC/build && cd $ATLAS_SRC/build && \
    $ATLAS_SRC/configure --prefix=$ATLAS_BIN --cripple-atlas-performance --shared && \
    make && \
    make install && \
    rm -rf /$ATLAS_PKG $ATLAS_SRC
    
# Compile and install FFTW
COPY $FFTW_PKG  /
RUN echo "" && \
    echo "============================================================" && \
    echo "==== Compiling and installing FFTW ====" && \
    cd $COTS_ROOT && tar xvzf /$FFTW_PKG && \
    mkdir -p $FFTW_BIN && cd $FFTW_SRC && \
    ./configure --prefix=$FFTW_BIN --enable-single --enable-threads && \
    make && \
    make install && \
    rm -rf /$FFTW_PKG $FFTW_SRC
    
# Compile and install SCAMP
COPY $SCAMP_PKG /
RUN echo "" && \
    echo "============================================================" && \
    echo "==== Compiling and installing SCAMP ====" && \
    cd $COTS_ROOT && unzip /$SCAMP_PKG && \
    mkdir -p $SCAMP_BIN && cd $SCAMP_SRC && \
    sh autogen.sh && \
    ./configure --prefix=$SCAMP_BIN \
        --with-atlas-libdir=$ATLAS_BIN/lib \
        --with-atlas-incdir=$ATLAS_BIN/include \
        --with-fftw-libdir=$FFTW_BIN/lib \
        --with-fftw-incdir=$FFTW_BIN/include \
        --with-curl-libdir=$CURL_LIB \
        --with-curl-incdir=$CURL_INC && \
    make && \
    make install && \
    rm -rf /$SCAMP_PKG $SCAMP_SRC 

# CLeanup and update environment
ENV PATH="${SCAMP_BIN}/bin:${ATLAS_BIN}/bin:${FFTW_BIN}/bin:${PATH}"
ENV LD_LIBRARY_PATH="${ATLAS_BIN}/lib:${FFTW_BIN}/lib:${PATH}"

