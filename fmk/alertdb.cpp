/******************************************************************************
 * File:    alertdb.cpp
 *          This file is part of QPF
 *
 * Domain:  qpf.fmk.DataManager
 *
 * Last update:  1.0
 *
 * Date:    20190614
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2019 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement DataManager class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "alertdb.h"

#include "libgen.h"

//----------------------------------------------------------------------
// Constructor
//----------------------------------------------------------------------
AlertDB::AlertDB()
{
}

//----------------------------------------------------------------------
// Destructor
//----------------------------------------------------------------------
AlertDB::~AlertDB()
{
}

//----------------------------------------------------------------------
// Method: instance
// Get the singleton instance
//----------------------------------------------------------------------
//AlertDB * AlertDB::instance()
//{
//    static AlertDB _instance;
//    return & _instance;
//}

//----------------------------------------------------------------------
// Method: set
// Set the data manager handler
//----------------------------------------------------------------------
void AlertDB::set(DataManager * _dataMng)
{
    AlertDB::dataMng = _dataMng;
}

//----------------------------------------------------------------------
// Method: raise
// Raise alert, shows it in the log, and stored in DB
//----------------------------------------------------------------------
void AlertDB::raise(Alert a, Logger & logger, Alert::Group grp)
{
    if (AlertDB::dataMng == nullptr) {
        logger.fatal("Data Manager not set, AlertDB not set up");
        return;
    }
    
    if (grp != Alert::Undefined) { a.setGroup(grp); }

    std::string alertMsg = a.dump();
    
    // Check that connection with the DB is possible
    try {
        dataMng->storeAlert(a);
    } catch (RuntimeException & e) {
        logger.error(std::string(__FILE__) + ":" + std::to_string(__LINE__) + ": " + e.what());
        return;
    }

    //  Store alert msg in log file
    if (a.getGroup() == Alert::Diagnostics) {
        if (a.getSeverity() > Alert::Warning) {
            logger.error(alertMsg);
        } else {
            logger.warn(alertMsg);
        }
    } else {
        if (a.getSeverity() == Alert::Error) {
            logger.error(alertMsg);
        } else if (a.getSeverity() > Alert::Fatal) {
            logger.fatal(alertMsg);
        } else {
            logger.warn(alertMsg);
        }
    }
    
}

DataManager * AlertDB::dataMng = nullptr;
