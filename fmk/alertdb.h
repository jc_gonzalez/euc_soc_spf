/******************************************************************************
 * File:    alertdb.h
 *          This file is part of QPF
 *
 * Domain:  qpf.tools.AlertDB
 *
 * Last update:  1.0
 *
 * Date:    20200216
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2015-2020 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare DataManager class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef ALERTDB_H
#define ALERTDB_H

//============================================================
// Group: External Dependencies
//============================================================

//------------------------------------------------------------
// Topic: System headers
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: External packages
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: Project headers
//------------------------------------------------------------
#include "alert.h"
#include "logging.h"
#include "datamng.h"

#define _NOOP()
#define _DEFER(x) x _NOOP()

//------------------------------------------------------------
// Topic: Alerts
//   - RaiseSysAlert(a)  - Raises system alert
//   - RaiseDiagAlert(a) - Raises diagnostics-related alert
//   - RaiseAlert(a)     - Raises alert with default scope
//------------------------------------------------------------

#define RaiseSysAlert(a)  AlertDB::raise(a, logger, Alert::System)
#define RaiseDiagAlert(a) AlertDB::raise(a, logger, Alert::Diagnostics)
#define RaiseAlert(a)     AlertDB::raise(a, logger)

#define CreateSysAlert(v, s) do {                                       \
        Alert *a = new Alert(Alert::System, v, Alert::General,          \
                             std::string(_DEFER(__FILE__)) + ":" +      \
                             std::to_string(_DEFER(__LINE__)), "", 0);  \
        Alert::Messages msgs;                                           \
        msgs.push_back(logger.getName() + ": " + s);                    \
        a->setMessages(msgs);                                           \
        AlertDB::raise(*a, logger, Alert::System);            \
    } while(0)

//==========================================================================
// Class: AlertDB
//==========================================================================
class AlertDB {

private:
    //----------------------------------------------------------------------
    // Constructor
    //----------------------------------------------------------------------
    AlertDB();

public:
    //----------------------------------------------------------------------
    // Destructor
    //----------------------------------------------------------------------
    ~AlertDB();

    //----------------------------------------------------------------------
    // Method: raise
    // Raise alert, shows it in the log, and stored in DB
    //----------------------------------------------------------------------
    static void raise(Alert a, Logger & logger, Alert::Group grp = Alert::Undefined);

    //----------------------------------------------------------------------
    // Method: instance
    // Get the singleton instance
    //----------------------------------------------------------------------
    //static AlertDB * instance();
    
    //----------------------------------------------------------------------
    // Method: set
    // Set the data manager handler
    //----------------------------------------------------------------------
    static void set(DataManager * _dataMng);
    
private:
    static DataManager * dataMng;
};

#endif // ALERTDB_H
