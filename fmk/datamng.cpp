/******************************************************************************
 * File:    datamng.cpp
 *          This file is part of QPF
 *
 * Domain:  qpf.fmk.DataManager
 *
 * Last update:  1.0
 *
 * Date:    20190614
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2019 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement DataManager class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "datamng.h"
#include "fv.h"
#include "alertdb.h"
#include "fnamespec.h"
#include "str.h"
#include "qdtrephdl.h"

//----------------------------------------------------------------------
// Constructor
//----------------------------------------------------------------------
DataManager::DataManager(Config & _cfg, ProcessingNetwork & _net)
    : cfg(_cfg), net(_net),
      logger(Logging::getLogger("datmng"))
{
}

//----------------------------------------------------------------------
// Destructor
//----------------------------------------------------------------------
DataManager::~DataManager()
{
}

//----------------------------------------------------------------------
// Method: initializeDB
// Initialize the DB
//----------------------------------------------------------------------
void DataManager::initializeDB()
{
    dbHdl = std::unique_ptr<DBHandler>(new DBHdlPostgreSQL(net, logger));

    dbHdl->setConnectionParams(cfg["db"]["host"].get<std::string>(),
                               std::stod(cfg["db"]["port"].get<std::string>()),
                               cfg["db"]["name"].get<std::string>(),
                               cfg["db"]["user"].get<std::string>(),
                               cfg["db"]["pwd"].get<std::string>());
    
    // Check that connection with the DB is possible
    if (dbHdl->openConnection()) {
        logger.info("Connection params. initialized for DB " +
                    cfg["db"]["name"].get<std::string>());
        dbHdl->closeConnection();
    }
}

//----------------------------------------------------------------------
// Method: storeAlert
//----------------------------------------------------------------------
void DataManager::storeAlert(Alert & a)
{
    try {
        // Check that connection with the DB is possible
        if (!dbHdl->openConnection()) {
            CreateSysAlert(Alert::Warning, "Cannot establish connection to database");
            return;
        }

        std::stringstream ss;
        ss << "INSERT INTO alerts "
           << "(alert_id, creation, grp, sev, typ, origin, msgs, file";
        if (a.getVar() != 0) { ss << ", var"; }
        char * bname = basename((char*)a.getFile().c_str());
        string orig(a.getOrigin());
        ss << ") VALUES ( nextval('alerts_alert_id_seq'), "
           << "'" << a.timeStampString() << "'" << ", "
           << "'" << Alert::GroupName[a.getGroup()] << "'" << ", "
           << "'" << Alert::SeverityName[a.getSeverity()] << "'" << ", "
           << "'" << Alert::TypeName[a.getType()] << "'" << ", "
           << "'" << str::replaceAll(orig, "'", "") << "'" << ", "
           << "'" << a.allMessages() << "'" << ", "
           << "'" << bname << "'";
        if (a.getVar() != 0) { ss << ", " << a.varAsTuple(); }
        ss << ");";

        dbHdl->runCmd(ss.str());
    } catch (RuntimeException & e) {
        CreateSysAlert(Alert::Fatal, e.what());
    }

    // Close connection
    dbHdl->closeConnection();
}

//----------------------------------------------------------------------
// Method: storeProducts
//----------------------------------------------------------------------
void DataManager::storeProducts(ProductMetaList & vm)
{
    try {
        // Check that connection with the DB is possible
        if (!dbHdl->openConnection()) {
            CreateSysAlert(Alert::Warning, "Cannot establish connection to database");
        }
        dbHdl->storeProducts(vm);
    } catch (RuntimeException & e) {
        CreateSysAlert(Alert::Fatal, e.what());
    }

    // Close connection
    dbHdl->closeConnection();
}

//----------------------------------------------------------------------
// Method: storeTaskInfo
//----------------------------------------------------------------------
bool DataManager::storeTaskInfo(string & taskId, int taskStatus,
                                string & taskInfo, bool initial)
{
    json taskData;

    try {
        // Check that connection with the DB is possible
        if (!dbHdl->openConnection()) {
            CreateSysAlert(Alert::Warning, "Cannot establish connection to database");
        }

        taskData = json::parse(taskInfo);
        
        // // Try to store the task data into the DB
        // if (initial) { dbHdl->storeTask(task); }
        // else         { dbHdl->updateTask(task); }

        if (!dbHdl->insertOrUpdateTask(taskId, taskStatus, taskData)) {
            CreateSysAlert(Alert::Warning, "Cannot store task " + taskId + " in database");
        }
    } catch (RuntimeException & e) {
        CreateSysAlert(Alert::Fatal, e.what());
    }

    // Close connection
    dbHdl->closeConnection();

    // If task is not finished, we are done
    if ((taskStatus != TASK_FINISHED) &&
        (taskStatus != TASK_FAILED)) { return false; }

    // Otherwise, task is finished, save outputs metadata

    // Check if there is any output product that needs further
    // processing (like QDT reports, which must be parsed and their
    // alerts extracted)
    static FileNameSpec fns;
    ProductMeta m;
    bool b;
    
    string outputStr = taskData["IO"]["output"].get<string>();
    vector<string> outputs = str::split(outputStr, ',');
    for (auto & f : outputs) {
        if (! fns.parse(f, m, b)) {
            CreateSysAlert(Alert::Error, "Cannot parse file name for product " + f);
            continue;
        }
        logger.info(fmt("Output $", m["url"]));
        try {
            if (((m["type"] == "QLA_VIS") || (m["type"] == "QLA_NIR") || (m["type"] == "QLA_SIR")) &&
                (m["format"] == "JSON")) {
                // Parse QDT report
                QDTReportHandler qdtRep(cfg["general"]["workArea"].get<string>() +
                                        "/data/archive/" +
                                        m["url"].get<string>().substr(7));
                if (!qdtRep.read()) continue;
                std::vector<Alert*> issues;
                qdtRep.getIssues(issues);
                if (issues.size() > 0) {
                    logger.warn(fmt("There are diagnostic alerts for product $",m["url"]));
                    for (auto & v : issues) { RaiseDiagAlert(*v); }
                }
            }
        } catch(...) {
            logger.error("Report alerts not imported");
        }
    }
    return true;
    
}

//----------------------------------------------------------------------
// Method: storeProductQueue
//----------------------------------------------------------------------
void DataManager::storeProductQueue(queue<ProductName> & q)
{

}

//----------------------------------------------------------------------
// Method: storeHostSpectra
//----------------------------------------------------------------------
void DataManager::storeHostSpectra(int hnum, dict & info)
{
    if ((hnum < 0) || (hnum >= net.numOfNodes)) { return; }
    
    try {
        // Check that connection with the DB is possible
        if (!dbHdl->openConnection()) {
            CreateSysAlert(Alert::Warning, "Cannot establish connection to database");
        }

        string & hName = net.nodeName.at(hnum);
        
        for (auto & kv: info["agents"].items()) {
            string agName = kv.key();
            //json const & agInfo = kv.value();
            TskStatSpectra tss = spectraJStoTss(kv.value());
            if (!dbHdl->saveTaskStatusSpectra(hName, agName, tss)) {
                CreateSysAlert(Alert::Warning,
                               fmt("Cannot store in DB the task spectra for agent $",
                                   agName));
            }
        }
    } catch (RuntimeException & e) {
        CreateSysAlert(Alert::Fatal, e.what());
    }

    // Close connection
    dbHdl->closeConnection();
}

//----------------------------------------------------------------------
// Method: spectraJStoTss
// Convert JSON spectra to TskStatSpectra structure
//----------------------------------------------------------------------
TskStatSpectra DataManager::spectraJStoTss(json & j)
{
    json & jj = j["spectrum"];
    return TskStatSpectra(jj["RUNNING"].get<int>(),
                          jj["SCHEDULED"].get<int>(),
                          jj["PAUSED"].get<int>(),
                          jj["STOPPED"].get<int>(),
                          jj["FAILED"].get<int>(),
                          jj["FINISHED"].get<int>());
}

//----------------------------------------------------------------------
// Method: getNewVersionForSignature
//----------------------------------------------------------------------
string DataManager::getNewVersionForSignature(string s)
{
    string newVer = "01.00";
    
    try {
        // Check that connection with the DB is possible
        if (!dbHdl->openConnection()) {
            CreateSysAlert(Alert::Warning, "Cannot establish connection to database");
        }

        string lastVer;
        if (dbHdl->checkSignature(s, lastVer)) {           
            FileVersion fver(lastVer);
            fver.incr();
            newVer = fver.getVersion();
        }
        
    } catch (RuntimeException & e) {
        CreateSysAlert(Alert::Fatal, e.what());
    }

    // Close connection
    dbHdl->closeConnection();

    return newVer;
}

//----------------------------------------------------------------------
// Method: lookForLocalArchFile
// Looks for a file in the local archive fulfilling the criteria
//----------------------------------------------------------------------
string DataManager::lookForLocalArchFile(string instr, string obsid,
                                         string exp, string type)
{
    string fileName("");
    
    try {
        // Check that connection with the DB is possible
        if (!dbHdl->openConnection()) {
            CreateSysAlert(Alert::Warning, "Cannot establish connection to database");
        }
        fileName = dbHdl->lookForLocalArchFile(instr, obsid, exp, type);        
    } catch (RuntimeException & e) {
        CreateSysAlert(Alert::Fatal, e.what());
    }

    // Close connection
    dbHdl->closeConnection();
    return fileName;
}
