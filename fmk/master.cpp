/******************************************************************************
 * File:    master.cpp
 *          This file is part of QPF
 *
 * Domain:  qpf.fmk.Master
 *
 * Last update:  1.0
 *
 * Date:    20190614
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2019 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement Master class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "master.h"

#include "alertdb.h"
#include "jsonfhdl.h"
#include "dwatcher.h"

#include <tuple>
#include <algorithm>
#include <random>
#include "limits.h"

#include "filetools.h"
#include "fnamespec.h"
#include "prodloc.h"
#include "str.h"

//----------------------------------------------------------------------
// Constructor
//----------------------------------------------------------------------
Master::Master(string _cfg, string _id, int _port, string _wa, int _bMode)
    : cfgFileName(_cfg),
      id(_id),
      workArea(_wa),
      port(_port),
      balanceMode(_bMode),
      wa(WorkArea(_wa)),
      lastNodeUsed(0),
      nodeInfoIsAvailable(false),
      logger(Logging::getLogger("master"))
{
    // Read configuration
    JsonFileHandler jFile(cfgFileName);
    if (!jFile.read()) {
        logger.fatal("Cannot open config. file '" + cfgFileName + "'. Exiting.");
    }
    cfg = jFile.getData();

    cfg["general"]["workArea"] = workArea;

    // Initialize processing network variables
    net = new ProcessingNetwork(cfg, id, balanceMode);
    logger.info("Node " + id + " has " + std::to_string(net->thisNodeNumOfAgents) +
            " processing agents");

    dist = new std::uniform_int_distribution<int>(0, net->numOfNodes - 1);

    masterLoopSleep_ms = cfg["general"]["masterHeartBeat"].get<int>();

    // Create task orchestrator and manager
    tskOrc = new TaskOrchestrator(cfg, id);
    tskMng = new TaskManager(cfg, id, wa, *net);

    // Create Data Manager
    if (net->thisIsCommander) {
        dataMng = new DataManager(cfg, *net);
        dataMng->initializeDB();
        AlertDB::set(dataMng);
    } else {
        dataMng = nullptr;
    }

    // Create HTTP server and requester object
    httpServer = new MasterServer(this, tskMng, port, wa);
    httpRqstr = new MasterRequester;

    logger.info("HTTP Server started at port " + std::to_string(port) +
                " (" + wa.serverBase + ")");

    // Initialize loads
    loads = VFOR(1.0, x, net->nodeName);

    // Create node selection function
    switch (balanceMode) {
    case BalancingModeEnum::BALANCE_Sequential:
        selectNodeFn = [](Master * m){
            return (m->lastNodeUsed + 1) % m->net->numOfNodes; };
        break;
    case BalancingModeEnum::BALANCE_LoadBalance:
        selectNodeFn = [](Master * m){
            int i = 0, imin = 0;
            double minLoad = 999.;
            for (auto x : m->loads) {
                if (x < minLoad) { imin = i, minLoad = x; }
                ++i;
            }
            return imin; };
        break;
    case BalancingModeEnum::BALANCE_Random:
        selectNodeFn = [](Master * m){ return m->genRandomNode(); };
        break;
    default:
        selectNodeFn = [](Master * m){ return - m->balanceMode - 1; };
    }
 
    // Launch server
    httpServer->launch();

    // Define start session
    startSession();    
}

//----------------------------------------------------------------------
// Destructor
//----------------------------------------------------------------------
Master::~Master()
{
    terminate();
}

//====[ ALL ]===========================================================

//----------------------------------------------------------------------
// Method: startSession
// Runs at: ALL
// Starts a new session (currently nothing is really done)
//----------------------------------------------------------------------
void Master::startSession()
{
    logger.info("Starting a new session with id " + wa.sessionId);
    logger.info("Session path is " + wa.sessionDir);
}

//----------------------------------------------------------------------
// Method: run
// Runs at: ALL
// Method to start the master execution
//----------------------------------------------------------------------
void Master::run()
{
    // Load State Vector
    loadStateVector();

    // Create Agents
    tskMng->createAgents();

    // Re-launch suspended tasks
    appendProdsToQueue( lookForSuspendedTasks() );

    // Create directory watchers
    setDirectoryWatchers();

    // Run main loop
    runMainLoop();

    // End
    terminate();
}

//----------------------------------------------------------------------
// Method: loadStateVector
// Runs at: ALL
// Load state vector (TBD)
//----------------------------------------------------------------------
void Master::loadStateVector()
{
}

//----------------------------------------------------------------------
// Method: lookForSuspendedTasks
// Runs at: ALL
// Look for suspended tasks from previous sessions
//----------------------------------------------------------------------
vector<string> & Master::lookForSuspendedTasks()
{
    return productsFromSuspTasks;
}

//----------------------------------------------------------------------
// Method: appendProdsToQueue
// Runs at: ALL
// Appends vector of products of previous tasks to queue of products to
// process
//----------------------------------------------------------------------
void Master::appendProdsToQueue(vector<string> & prods)
{
    for (auto & fileName: prods) { productList.push(std::move(fileName)); }
    prods.clear();
}

//----------------------------------------------------------------------
// Method: appendProdsToQueue
// Runs at: ALL
// Appends queue of products of previous tasks to queue of products to
// process
//----------------------------------------------------------------------
void Master::appendProdsToQueue(Queue<string> & prods, bool atBeginning)
{
    if (! atBeginning) {
        std::string fileName;
        while (prods.get(fileName)) {
            productList.push(std::move(fileName));
        }
    } else {
        productList.prepend(prods);
    }
}

//----------------------------------------------------------------------
// Method: setDirectoryWatchers
// Runs at: ALL
// Sets the directory watchers for reprocessing and for the inbox
//----------------------------------------------------------------------
void Master::setDirectoryWatchers()
{
    // Nominal input directory - data/inbox
    dirWatchers.push_back(DirWatchedAndQueue(new DirWatcher(wa.localInbox),
                                             inboxProdQueue));
    // Reprocessing directory - data/reproc
    dirWatchers.push_back(DirWatchedAndQueue(new DirWatcher(wa.reproc),
                                             reprocProdQueue));

    if (net->thisIsCommander) {
        // External product injection (schedule product) - server/proc/schedule
        dirWatchers.push_back(DirWatchedAndQueue(new DirWatcher(wa.extSchedule),
                                                 extScheduleQueue));
        
        // External product injection (process now) - server/proc/process
        dirWatchers.push_back(DirWatchedAndQueue(new DirWatcher(wa.extProcess),
                                                 extProcessQueue));
    }
}

//----------------------------------------------------------------------
// Method: runMainLoop
// Runs at: ALL
// Main loop
//----------------------------------------------------------------------
void Master::runMainLoop()
{
    logger.info("Start!");
    int iteration = 1;

    // Get start time
    auto start_time = std::chrono::steady_clock::now();
    // Compute end time
    auto next_time = start_time + std::chrono::seconds(1);

    forever {

        logger.debug("Iteration " + std::to_string(iteration));

        // Collect new products to process
        if (getNewEntries()) {
            appendProdsToQueue(reprocProdQueue);
            appendProdsToQueue(inboxProdQueue);

            if (net->thisIsCommander) {
                // FOR COMMANDER
                appendProdsToQueue(extScheduleQueue);
                appendProdsToQueue(extProcessQueue, true);
            }
        }

        // Schedule the processing
        if (!productList.empty()) {
            scheduleProductsForProcessing();
        }

        // Retrieve agents information
        if ((iteration == 1) || ((iteration % 5) == 0)) {
            nodeInfoIsAvailable = tskMng->retrieveAgentsInfo(nodeInfo);
            //logger.debug("agentsInfo == nodeInfo := <<<\n" + getNodeInfo() + "\n>>>");
            tskMng->showSpectra();
        }

        // Retrieve pending outputs
        tskMng->retrieveOutputs(outputProducts);
        //outputProducts.dump();

        if (net->thisIsCommander) {
            // FOR COMMANDER

            // Place products in outputProducts list into the archive (and DB)
            archiveOutputs();
        } else {
            // FOR REMOTE NODES

            // 1. Takes all files in data/archive, and transfer them to commander
            //      REMOTE:data/archive  ==>  COMMANDER:server/outputs
            transferRemoteLocalArchiveToCommander();

            // 2. Transfer files declared as outputs to commander
            //      REMOTE:data/archive  ==>  COMMANDER:server/outputs
            transferOutputsToCommander();
        }

        // Update tasks information
        tskMng->updateTasksInfo();

        // Retrieve nodes information
        if ((net->thisIsCommander) && ((iteration % 10) == 1)) {
            // COMMANDER
            gatherNodesStatus();
            gatherTasksStatus();

            storeAgentsSpectra();
        }

        // Wait a little bit until next loop
        while (next_time < std::chrono::steady_clock::now()) {
            next_time += std::chrono::seconds(1);
            ++iteration;
        }
        std::this_thread::sleep_until(next_time);
    }
}

//----------------------------------------------------------------------
// Method: getNewEntries
// Runs at: ALL
// Get all new entries
//----------------------------------------------------------------------
bool Master::getNewEntries()
{
    bool weHaveNewEntries = false;
    DirWatcher * dw;
    Queue<string> q;
    for (DirWatchedAndQueue grp : dirWatchers) {
        DirWatcher * dw = std::get<0>(grp);
        Queue<string> & q = std::get<1>(grp);
        weHaveNewEntries |= getNewEntriesFromDirWatcher(dw, q);
    }
    return weHaveNewEntries;
}

//----------------------------------------------------------------------
// Method: getNewEntriesFromDirWatcher
// Runs at: ALL
// Get new entries from a given directory watcher
//----------------------------------------------------------------------
bool Master::getNewEntriesFromDirWatcher(DirWatcher * dw, Queue<string> & q)
{
    // Process new events, at most 5 per iteration
    static const int numMaxEventsPerIter = 5;

    DirWatcher::DirWatchEvent e;
    int numEvents = 0;
    while ((dw->nextEvent(e)) && (numEvents < numMaxEventsPerIter)) {
        logger.info("New DirWatchEvent: " + e.path + "/" + e.name
                + (e.isDir ? " DIR " : " ") + std::to_string(e.mask));

        // Process only files
        // TODO: Process directories that appear at inbox
        if (! e.isDir) {
            // Build full file name and add it to the queue
            q.push(fmt("$/$", e.path, e.name));
            ++numEvents;
        }
    }
    return (numEvents > 0);
}

//----------------------------------------------------------------------
// Method: scheduleProductsForProcessing
// Runs at: ALL
// Schedule products for processing in one of the proc. nodes
//----------------------------------------------------------------------
void Master::scheduleProductsForProcessing()
{
    if (net->thisIsCommander) {
        // COMMANDER: Distribute among all the nodes
        distributeProducts();
    } else {
        // REMOTE: Process all the products in the list
        productsForProcessing.append(productList);
    }

    ProductName prod;
    ProductMeta meta;
    bool b;
    ProductMetaList products;
    vector<string> productsToRemove;

    // Take and handle the products in processing list
    while (productsForProcessing.get(prod)) {
        productsToRemove.push_back(prod);
        
        if (! checkIfProduct(prod, meta, b)) {
            logger.warn("File '" + prod + "' doesn't seem to be a valid product");
            continue;
        }

        logger.info("Product '" + prod + "' will be processed");

        logger.debug(fmt("Try to archive product $", prod));
        if (!ProductLocator::toLocalArchive(meta, wa)) {
            logger.error("Move (link) to archive of %s failed", prod.c_str());
            continue;
        }

        products.push_back(meta);

        if (! tskOrc->schedule(meta, *tskMng)) {
            logger.error("Couldn't schedule the processing of %s", prod.c_str());
            continue;
        }
    }

    // The inputs products dispatched to other nodes only have to be
    // then archived in the local archive (in case this is the commander)
    if (net->thisIsCommander) {
        // Check products for archival
        while (productsForArchival.get(prod)) {
            productsToRemove.push_back(prod);
            
            logger.info("Product for archival: " + prod);
            if (! checkIfProduct(prod, meta, b)) {
                logger.warn("File '" + prod + "' doesn't seem to be a valid product");
                continue;
            }
            products.push_back(meta);
        }

        // Store them in the DB
        dataMng->storeProducts(products);
    }

    logger.info("Cleaning inbox . . .");
    for (auto & prod: productsToRemove) { (void)unlink(prod.c_str()); }
}

//----------------------------------------------------------------------
// Method: terminate
// Runs at: ALL
// Delete handlers amnd cleanup
//----------------------------------------------------------------------
void Master::terminate()
{
    // Destroy all elements
    delete httpRqstr;
    delete httpServer;
    delete tskMng;
    delete tskOrc;
    if (net->thisIsCommander) delete dataMng;

    logger.info("Done.");
}

//====[ COMMANDER ]=====================================================

//----------------------------------------------------------------------
// Method: gatherNodesStatus
// Runs at: COMMANDER
// Collects all nodes agents info
//----------------------------------------------------------------------
void Master::gatherNodesStatus()
{
    nodeStatus.clear();
    nodeStatusIsAvailable.clear();

    for (auto & node: net->nodesButComm) {
        auto it = std::find(net->nodeName.begin(),
                            net->nodeName.end(), node);
        int i = it - net->nodeName.begin();

        string url(net->nodeServerUrl.at(i));
        httpRqstr->setServerUrl(url);
        string resp;
        if (!httpRqstr->requestData("/status", resp)) {
            logger.warn("Couldn't get node '%s' information", node.c_str());
            nodeStatus.push_back(json{-1});
            nodeStatusIsAvailable.push_back(false);
            continue;
        }

        //logger.debug(fmt("$ ($ /status) => nodeStatus := $", node, url, resp));
        //CreateSysAlert(Alert::Warning, "NodeStatus gathered: " + node + " := " + resp);

        json respObj;
        try {
            respObj = json::parse(resp);
        } catch(...) {
            logger.warn("Problems in the translation of node '%s' "
                        "information", node.c_str());
            nodeStatus.push_back(json{-1});
            nodeStatusIsAvailable.push_back(false);
            continue;
        }

        nodeStatus.push_back(respObj);
        nodeStatusIsAvailable.push_back(true);
    }
}

//----------------------------------------------------------------------
// Method: gatherTasksStatus
// Runs at: COMMANDER
// Collects all nodes tasks info
//----------------------------------------------------------------------
void Master::gatherTasksStatus()
{
	static vector<string> storedTasks;
	logger.info("List of stored tasks:");
	for (auto & s: storedTasks) { logger.info(" - " + s); }
	
    int i = -1;
    for (auto & node: net->nodeName) {
        ++i;
        httpRqstr->setServerUrl(net->nodeServerUrl.at(i));
        string resp;
        if (!httpRqstr->requestData("/tstatus", resp)) {
            logger.warn("Couldn't get node '%s' information", node.c_str());
            continue;
        }
        //logger.debug("/tstatus => tasksInfo := <<<\n" + resp + "\n>>>");

        if (resp == "{}") { continue; }

        json respObj;
        try {
            respObj = json::parse(resp);
        } catch(...) {
            logger.warn("Problems in the translation of tasks info. from node '%s'",
                        node.c_str());
            continue;
        }

        // Retrieve info for tasks on the node, and store it into DB
        for (auto & kv: respObj.items()) {
            string const & agName = kv.key();
            json const & agTaskInfo = kv.value();

            string tid = agTaskInfo["task_id"];
            string tinfo = agTaskInfo["info"].dump();
            string tstatus = agTaskInfo["status"];
            int statusVal = TaskStatusVal[tstatus];
            bool isNew = agTaskInfo["new"];

            if (std::find(storedTasks.begin(), storedTasks.end(), tid) == storedTasks.end()) {
                logger.info("Storing at archive task info for task: " + tid);
                if (dataMng->storeTaskInfo(tid, statusVal, tinfo, isNew)) {
                    storedTasks.insert(storedTasks.begin(), tid); 
                }
            }	
        }
    }
}

//----------------------------------------------------------------------
// Method: storeAgentsSpectra
// Runs at: COMMANDER
// With the help of the Data Manager, stores all agents spectra in DB
//----------------------------------------------------------------------
void Master::storeAgentsSpectra()
{
    int i = 0;
    
    // First, store Commander agents spectra
    dataMng->storeHostSpectra(i, nodeInfo);
        
    // Then, store all the Processing Nodes' agents spectra
    for (auto & nInfo: nodeStatus) {
        dataMng->storeHostSpectra(++i, nInfo);
    }
}

//----------------------------------------------------------------------
// Method: distributeProducts
// Runs at: COMMANDER
// Distribute products among all the procesing nodes
//----------------------------------------------------------------------
void Master::distributeProducts()
{
    // Update load averages for all the nodes
    updateLoads();

    ProductName prod;
    ProductMeta meta;
    bool needsVersion;

    while (productList.get(prod)) {
        int numOfNodeToUse = selectNodeFn(this);
        string nodeToUse = net->nodeName[numOfNodeToUse];
        bool processInThisNode = nodeToUse == id;

        if (! checkIfProduct(prod, meta, needsVersion)) {
            logger.warn("File '" + prod + "' doesn't seem to be a valid product");
            continue;
        } else {
            // If it is a JSON file, we assume it is a QLA report, so we will use the
            // current node to process it
            // TODO: Change to check the actual type of the product
            // In this case, the version will already be in the file name, so we
            // can skip next "if"
//            if ("JSON" == meta["format"].get<string>()) {
//                logger.info("We have a JSON: " + prod);
//                numOfNodeToUse = net->commanderNum;
//                nodeToUse = id;
//                processInThisNode = true;
//            } else {
	            if (needsVersion) {
    	            string newVersion = dataMng->getNewVersionForSignature(meta["instance"]);
        	        json & fs = meta["fileinfo"];
            	    string folder = fs["path"].get<string>();
                	string newName = (fs["sname"].get<string>() + "_" + newVersion +
                    	              "." + fs["ext"].get<string>());
    	            string newProd = folder + "/" + newName;
        	        logger.debug("Changing name from " + prod + " to " + newProd);
            	    if (rename(prod.c_str(), newProd.c_str()) != 0) {
                	    logger.error("Couldn't add version tag to product " + prod);
     	           }
        	        // The new, renamed file will be captured by the DirWatcher, so we forget
            	    // about this prod
                	continue;
            	}
//            }
        }

        logger.debug("Processing of " + prod + " will be done by node " + nodeToUse);

        if (! processInThisNode) {
            // If the processing node is not the commander (I'm the
            // commander in this function), dispatch it to the
            // selected node, and save it to remove it from the list
            httpRqstr->setServerUrl(net->nodeServerUrl[numOfNodeToUse]);
            if (httpRqstr->postFile("/inbox", prod, "application/octet-stream")) {
                productsForArchival.push(std::move(prod));
            } else {
                logger.error("Cannot send file %s to node %s",
                             prod.c_str(), nodeToUse.c_str());
                logger.warn("Product will be processed by master node");
                processInThisNode = true;
                numOfNodeToUse = net->commanderNum;
                nodeToUse = id;
            }
        }

        if (processInThisNode) {
            productsForProcessing.push(std::move(prod));
        }

        lastNodeUsed = numOfNodeToUse;
    }
}

//----------------------------------------------------------------------
// Method: updateLoads
// Runs at: COMMANDER
// Updates the average loading values for all the hosts
//----------------------------------------------------------------------
void Master::updateLoads()
{
    for (int i = 0; i < nodeStatus.size(); ++i) {
        if (nodeStatusIsAvailable[i]) {
            try {
                json jloads = nodeStatus[i]["machine"]["load"];
                loads[i] = jloads[0].get<double>();
            } catch (...) {
                loads[i] = 1.0;
            }
        }
    }
}

//----------------------------------------------------------------------
// Method: archiveOutputs
// Run at: COMMANDER
// Save output products to the local archive
//----------------------------------------------------------------------
void Master::archiveOutputs()
{
    ProductName prod;
    ProductMeta meta;
    ProductMetaList products;
    bool needsVersion;
    while (outputProducts.get(prod)) {
        if (checkIfProduct(prod, meta, needsVersion)) {
            ProductLocator::toLocalArchive(meta, wa, ProductLocator::MOVE);
            logger.debug(fmt("$:$: Try to archive product $",
                             __FUNCTION__, __LINE__, meta["url"].get<string>()));
            products.push_back(meta);
        } else {
            logger.warn("Found non-product file in local outputs folder: " + prod);
        }
    }
    if (products.size() > 0) {
        dataMng->storeProducts(products);
    }
}

//====[ REMOTE ]========================================================

//----------------------------------------------------------------------
// Method: transferRemoteLocalArchiveToCommander
// Run at: REMOTE  (REMOTE:data/archive  ==>  COMMANDER:server/outputs)
// Transfer (POST) outputs to server/outputs end of commander server
//----------------------------------------------------------------------
void Master::transferRemoteLocalArchiveToCommander()
{
    Queue<string> archProducts;
    for (auto & s: FileTools::filesInFolder(wa.archive)) {
        if (str::getExtension(s) == "fits") {
            unlink(s.c_str());
        } else {
            string prod(s);
            archProducts.push(std::move(prod));
        }
    }
    transferFilesToCommander(archProducts, "/outputs");
}

//----------------------------------------------------------------------
// Method: transferOutputsToCommander
// Run at: REMOTE
// Transfer (POST) outputs to server/outputs end of commander server
//----------------------------------------------------------------------
void Master::transferOutputsToCommander()
{
    transferFilesToCommander(outputProducts, "/outputs");
}

//----------------------------------------------------------------------
// Method: transferFilesToCommander
// Run at: REMOTE
// Transfer (POST) outputs to server/XXX end of commander server
//----------------------------------------------------------------------
void Master::transferFilesToCommander(Queue<string> & prodQueue,
                                      string route)
{
    ProductName prod;
    while (prodQueue.get(prod)) {
        logger.info("Transfer of product " + prod + " for archival\n\n\n\n");
        httpRqstr->setServerUrl(net->commanderUrl);
        if (!httpRqstr->postFile(route, prod,
                                 "application/octet-stream")) {
            logger.error("Cannot send file " + prod + " to " + net->commander);
            continue;
        }
        unlink(prod.c_str());
    }
}

//====[ MINOR FUNCTIONS ]===============================================

//----------------------------------------------------------------------
// Method: checkIfProduct
//
//----------------------------------------------------------------------
bool Master::checkIfProduct(string & fileName, ProductMeta & meta,
                            bool & needsVersion)
{
    static FileNameSpec fns;
    return fns.parse(fileName, meta, needsVersion);
}

//----------------------------------------------------------------------
// Method: getNodeInfo
//
//----------------------------------------------------------------------
string Master::getNodeInfo()
{
    if (nodeInfoIsAvailable) {
        return nodeInfo.dump(); //nodeInfo.dump();
    } else {
        return "{}";
    }
}

//----------------------------------------------------------------------
// Method: genRandomNode
// Generated the 0-base index of a Node, randomly
//----------------------------------------------------------------------
int Master::genRandomNode()
{
    return (*dist)(mt);
}

//----------------------------------------------------------------------
// Method: delay
// Waits for a small time lapse for system sync
//----------------------------------------------------------------------
void Master::delay(int ms)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

//----------------------------------------------------------------------
// Method: lookForLocalArchFile
// Looks for a file in the local archive fulfilling the criteria
//----------------------------------------------------------------------
string Master::lookForLocalArchFile(string instr, string obsid,
                                    string exp, string type)
{
    return dataMng->lookForLocalArchFile(instr, obsid, exp, type); 
}


std::mt19937 Master::mt;

