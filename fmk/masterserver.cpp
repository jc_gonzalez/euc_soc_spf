/******************************************************************************
 * File:    masterserver.cpp
 *          This file is part of QPF
 *
 * Domain:  qpf.fmk.MasterServer
 *
 * Last update:  1.0
 *
 * Date:    20190614
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2019 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement MasterServer class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "masterserver.h"
#include "master.h"
#include "taskmng.h"
#include "filetools.h"
#include "str.h"
#include "prodloc.h"

#include <memory>
#include <fstream>
#include <thread>
#include <libgen.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/* ------------------------------------------------------------

   # List of routes handled by this server
   
   This is the list of routes handled by this server, along with the
   methods (HTTP Verbs) defined specifically to handle the
   communication through that route:
   
   - /hello (GET) 

   Just a sample route, to see if server is launched

   - /status (GET) 

   Provides access from the clients to the Host information structure
   
   - /tstatus (GET)

   Provides task status structures
   
   - /log/{logfile} (GET)

   Requests log file with that name.  If {logfile} is "logfiles"
   (without quotes) a list of the available log files is returned as a
   JSON stream
   
   - /inbox/{prod} (POST) 

   Receives a product file to be stored in the server/inbox folder
   
   - /outputs/{prod} (POST) 

   Receives a product file to be stored in the server/outputs folder
   
   - /proc/schedule/{prod} (POST) 

   Receives a product file to be stored in the server/proc/schedule
   folder, to add it to the processing queue
   
   - /proc/process/{prod} (POST) 

   Receives a product file to be stored in the server/proc/process
   folder, to place it at the BEGINNING of the processing queue
   
   - /archive/<instr>/<obsid>/<exp>/{type:le1,qla,log}/{item:name,data} (GET)

   Receives a product file to be stored in the server/proc/process
   folder, to place it at the BEGINNING of the processing queue
   
   ------------------------------------------------------------
*/

class RscHelloWorld : public http_resource {
public:
    const HttpRespPtr render_GET(const http_request&) {
        return HttpRespPtr(new strResp("GET: Hello, World!"));
    }

    const HttpRespPtr render(const http_request&) {
        return HttpRespPtr(new strResp("OTHER: Hello, World!"));
    }
};

class RscMultiple : public http_resource {
public:
    const HttpRespPtr render(const http_request& req) {
        return HttpRespPtr(new strResp("Your URL: " + req.get_path()));
    }
};

class RscArgs : public http_resource {
public:
    const HttpRespPtr render(const http_request& req) {
        return HttpRespPtr(new strResp("ARGS: " + req.get_arg("arg1") +
                                       " and " + req.get_arg("arg2")));
    }
};

class RscHostStatus : public http_resource {
public:
    void setMasterHdl(Master * hdl) { mhdl = hdl; }
    
    const HttpRespPtr render_GET(const http_request&) {
        return HttpRespPtr(new strResp(mhdl->getNodeInfo(), 200,
                                       "application/json"));
    }

    const HttpRespPtr render(const http_request&) {
        return HttpRespPtr(new strResp("", 404));
    }
private:
    Master * mhdl;
};

class RscLogFile : public http_resource {
public:
    void setWorkArea(WorkArea * _wa) { wa = _wa; }
    const HttpRespPtr render_GET(const http_request& rqst) {
        char * bname = basename((char*)(rqst.get_path().c_str()));
        string baseName(bname);
        if (baseName == "logfiles") {
            std::vector<string> logfiles = FileTools::filesInFolder(wa->wwwlog, "log");
            for (auto & s: logfiles) {
                string ss("\"" + string(basename((char*)(s.c_str()))) + "\"");
                s = ss;
            } 
            string jlogFiles = ("{\"logfiles\": [" +
                                     str::join(logfiles, ", ") + "]}");
            return HttpRespPtr(new strResp(jlogFiles, 200,
                                           "application/json"));
        }
        string logfile = wa->wwwlog + "/" + string(bname);
        struct stat buf;
        if (FileTools::exists(logfile)) {
            return FileRespPtr(new fileResp(logfile, 200,
                                            "text/plain"));
        } else {
            return HttpRespPtr(new strResp("", 404));
        }
    }

    const HttpRespPtr render(const http_request&) {
        return HttpRespPtr(new strResp("", 404));
    }
private:
    WorkArea * wa;
};

class RscTaskStatus : public http_resource {
public:
    void setTaskMngHdl(TaskManager * hdl) { thdl = hdl; }
    
    const HttpRespPtr render_GET(const http_request&) {
        return HttpRespPtr(new strResp(thdl->getTaskInfo(), 200,
                                       "application/json"));
    }

    const HttpRespPtr render(const http_request&) {
        return HttpRespPtr(new strResp("", 404));
    }
private:
    TaskManager * thdl;
};

class RscInOutReceiver : public http_resource {
public:
    void setWorkArea(WorkArea * _wa) { wa = _wa; }
    
    const HttpRespPtr render_POST(const http_request&rqst) {
        std::vector<string> pathItems = rqst.get_path_pieces();

        // Save content to local file in server folder
        string fullFileName = wa->serverBase + rqst.get_path();
        std::ofstream fout(fullFileName);
        fout << rqst.get_content();
        fout.close();

        // Move created file to local inbox
        if (pathItems.at(0) == "inbox") {
            string newFullFileName = wa->localInbox + "/" + pathItems.at(1);
            int res = ProductLocator::relocate(fullFileName, newFullFileName,
                                               ProductLocator::MOVE);
        }
        
        return HttpRespPtr(new strResp("Done.", 200));
    }

    const HttpRespPtr render(const http_request&) {
        return HttpRespPtr(new strResp("", 404));
    }
private:
    WorkArea * wa;
};

class RscExtSchedReceiver : public http_resource {
public:
    void setWorkArea(WorkArea * _wa) { wa = _wa; }


    const HttpRespPtr render_POST(const http_request&rqst) {
        std::vector<string> pathItems = rqst.get_path_pieces();

        // Save content to local file in server folder
        string fullFileName = wa->serverBase + rqst.get_path();
        std::ofstream fout(fullFileName);
        fout << rqst.get_content();
        fout.close();

        return HttpRespPtr(new strResp("Done.", 200));
    }
    const HttpRespPtr render(const http_request&) {
        return HttpRespPtr(new strResp("", 404));
    }
private:
    WorkArea * wa;
};

class RscLocalArchFile : public http_resource {
public:
    void setMasterHdl(Master * hdl) { mhdl = hdl; }
    void setWorkArea(WorkArea * _wa) { wa = _wa; }
    
    const HttpRespPtr render_GET(const http_request& rqst) {
        // URL is /archive/<instr>/<obsid>/<exp>/<type:{le1,qla,log}>/<item:{name,data}>
        string instr = rqst.get_arg("instr");
        string obsid = rqst.get_arg("obsid");
        string exp   = rqst.get_arg("exp");
        string type  = rqst.get_arg("type");
        string item  = rqst.get_arg("item");

        // Make request of file name
        string fileName = mhdl->lookForLocalArchFile(instr, obsid, exp, type);       
        if (fileName.empty()) {
            return HttpRespPtr(new strResp("", 404));
        }

        string fullFileName = wa->archive + "/" + fileName;
        struct stat buf;
        if (FileTools::exists(fullFileName)) {
            return ((item == "name") ?
                    HttpRespPtr(new strResp(fileName, 200)) :
                    FileRespPtr(new fileResp(fullFileName, 200,
                                             "application/octet-stream")));
        } else {
            return HttpRespPtr(new strResp("", 404));
        }
    }

    const HttpRespPtr render(const http_request&) {
        return HttpRespPtr(new strResp("", 404));
    }
private:
    Master * mhdl;
    WorkArea * wa;
};

//----------------------------------------------------------------------
// Constructor
//----------------------------------------------------------------------
MasterServer::MasterServer(Master * hdl, TaskManager * hdlt, int prt, WorkArea & _wa)
    : HttpCommServer(prt, _wa.serverBase), mhdl(hdl), thdl(hdlt), wa(_wa)
{
}

//----------------------------------------------------------------------
// Destructor
//----------------------------------------------------------------------
MasterServer::~MasterServer()
{
}

//----------------------------------------------------------------------
// Method: launch
// Initialize and launch the HTTP server
//----------------------------------------------------------------------
void MasterServer::launch()
{
    std::thread(&MasterServer::run, this).detach();
}

//----------------------------------------------------------------------
// Method: launch
// Initialize and launch the HTTP server
//----------------------------------------------------------------------
void MasterServer::run()
{
    webserver ws = create_webserver(port)
        .content_size_limit(45069760); // cw;

    RscHelloWorld rscHello;
    addRoute(ws, "/hello", &rscHello);

    RscHostStatus rscStatus;
    rscStatus.setMasterHdl(mhdl);
    addRoute(ws, "/status", &rscStatus);

    RscTaskStatus rscTStatus;
    rscTStatus.setTaskMngHdl(thdl);
    addRoute(ws, "/tstatus", &rscTStatus);

    RscLogFile rscLogFile;
    rscLogFile.setWorkArea(&wa);
    addRoute(ws, "/log/{logfile}", &rscLogFile);

    RscInOutReceiver rscInOutRcv;
    rscInOutRcv.setWorkArea(&wa);
    addRoute(ws, "/inbox/{prod}", &rscInOutRcv);
    addRoute(ws, "/outputs/{prod}", &rscInOutRcv);

    RscExtSchedReceiver rscExtSchedRcv;
    rscExtSchedRcv.setWorkArea(&wa);
    addRoute(ws, "/proc/schedule/{prod}", &rscExtSchedRcv);
    addRoute(ws, "/proc/process/{prod}", &rscExtSchedRcv);

    RscLocalArchFile rscLocalArchFile;
    rscLocalArchFile.setMasterHdl(mhdl);
    rscLocalArchFile.setWorkArea(&wa);
    addRoute(ws, "//archive/{instr}/{obsid}/{exp}/{type}/{item}", &rscLocalArchFile);
    
    ws.start(true);
}

