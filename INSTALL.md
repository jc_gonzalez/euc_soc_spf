SPF - The Euclid SOC Processing Framework
================================================

SPF is a C++ application that makes use of many of the C++-11 new
features of the C++ language.  It was originally developed in an
Ubuntu 15.10 box with a GCC compiler version 5.2.1 (Ubuntu
5.2.1-22ubuntu2 - 20151010).  From release V1.1 on, the development
has been done using a LODEEN 1.2 (based on CentOS 7) platform as a
compilation, installation and testing machine.

There is a companion application, the SPF Archive Viewer, which is
also a C++ application and it is used as Graphical User Interface to
view the content of the SPF local archive.  It is based on Qt 5.x.

The SPF is an application that can be deployed in a multinode setup.
One of the nodes is the Master node, and the rest are Processing
nodes.  The Master node commands the rest of the nodes the files that
have to process, stores the required information in the internal
database, and hold the Local Archive.  It is in the Master node where
the SPF Archive Viewer can be executed.

Installation
-----------------------

The following sections show the different steps required for the
installation of the SPF in the Master node as well as in a Processing
node.

### Getting the code

For the retrieval of the code and its compilation you will need at least the following:

- Git
- CMake 
- Make
- C++ compiler that supports C++11 (a reasonably recent version of Gcc or Clang will do).

In order to get the code, you just have to clone the Git repository in
a folder on the machine you want to install SPF.

    $ git clone --recurse-submodules <SPF-repository-url>

We will use the env. variable SPFBASE as the name of the full directory path where the repository where cloned.  Then, after cloning, we set the value of this variable:

    $ cd spf
    $ export SPFBASE=$(pwd)

### Installation of COTS

In addition, there are some COTS packages that must be installed in order to allow
the operation of the SPF.  Some of them are required at compilation,
some of them must be installed for the execution of the application.
They are the following:

- PostgreSQL
- LibCurl
- UUID library
- Docker
- LibMicroHTTPD
- LibLog4C++
- Qt (only for the SPF Archive Viewer)

The installation of COTS can be easily done by running the following
Bash script, under the `scripts/install` subfolder:

    $ $SPFBASE/scripts/install/InstallCOTS.sh -h

The easiest way to use it is with the option `-A` to install all COTS (including Qt) for the *Master* node, and `-a` to install all the COTS (excluding Qt) for the *Processing* nodes.

### Compilation of the SPF code

For the compilation of the code, we can use the following script:

    $ $SPFBASE/scripts/install/Build.sh -h

The first time you will probably want to use the options `-c`, `-r`, `-w` and `-b`.  Once the SPF is compiled, you can use `-i` to install it.  The default installation is done in `$HOME/.local`.

More information
-----------------------

For a detailed explanation of the procedure to install QPF (and the
QDT as well), please refer to the [SOC Processing Framework and
Diagnostic Tool Installation][1] guide.


[1]: https://issues.cosmos.esa.int/euclidwiki/display/QLA/QLA+Processing+Framework+and+Diagnostic+Tool+Installation)
